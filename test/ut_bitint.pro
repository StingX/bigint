TEMPLATE = app

CONFIG += console warn_on depend_includepath testcase c++11
CONFIG -= app_bundle

QT = testlib core

SOURCES += \
	../Bigint.cpp \
    ut_bigint.cpp

HEADERS += \
	../Bigint.h

INCLUDEPATH += ../
