/** \file ut_bigint.cpp
  * \brief Unit tests for the Bigint
  * \author Devyatnikov A.
  * \date 04.06.2016
  * Project: QPromise
  */

#include "Bigint.h"

#include <QtTest>

#define GCD_PAIR_COUNT  3

static const int gcd_pairs[GCD_PAIR_COUNT][3] =
{
    { 693, 609, 21 },
    { 1764, 868, 28 },
    { 768454923, 542167814, 1 }
};

namespace QTest
{
inline bool qCompare(Bigint const &t1, Bigint const &t2, const char *actual, const char *expected,
                    const char *file, int line)
{
    return compare_helper(t1 == t2, "Compared values are not the same",
                          toString(t1.toQString()), toString(t2.toQString()), actual, expected, file, line);
}
}


// Helpers
template <typename Tint> Tint bigint2int(const Bigint& x, bool* ok);
template <> qint16 bigint2int<qint16>(const Bigint& x, bool* ok) { return x.toInt16(ok); }
template <> quint16 bigint2int<quint16>(const Bigint& x, bool* ok) { return x.toUInt16(ok); }
template <> qint32 bigint2int<qint32>(const Bigint& x, bool* ok) { return x.toInt32(ok); }
template <> quint32 bigint2int<quint32>(const Bigint& x, bool* ok) { return x.toUInt32(ok); }
template <> qint64 bigint2int<qint64>(const Bigint& x, bool* ok) { return x.toInt64(ok); }
template <> quint64 bigint2int<quint64>(const Bigint& x, bool* ok) { return x.toUInt64(ok); }

// Data for test function, that checks integer in/out handling
template <typename Tint>
void integertest_data()
{
  QTest::addColumn<Bigint>("source");
  QTest::addColumn<bool>("expected_ok");
  QTest::addColumn<Tint>("expected_result");

  QTest::newRow("empty") << Bigint() << true << Tint(0);

  QTest::newRow("valid_min+1")
      << (Bigint::fromInt(std::numeric_limits<Tint>::min()) + Bigint(1))
      << true
      << Tint(std::numeric_limits<Tint>::min() + 1);

  QTest::newRow("valid_min")
      << Bigint::fromInt(std::numeric_limits<Tint>::min())
      << true
      << Tint(std::numeric_limits<Tint>::min());

  if (std::numeric_limits<Tint>::is_signed)
  {
    QTest::newRow("valid_min_half")
        << Bigint::fromInt(std::numeric_limits<Tint>::min() / 2)
        << true
        << Tint(std::numeric_limits<Tint>::min() / 2);
  }

  QTest::newRow("valid_max-1")
      << (Bigint::fromInt(std::numeric_limits<Tint>::max()) - Bigint(1))
      << true
      << Tint(std::numeric_limits<Tint>::max() - 1);

  QTest::newRow("valid_max")
      << Bigint::fromInt(std::numeric_limits<Tint>::max())
      << true
      << Tint(std::numeric_limits<Tint>::max());

  QTest::newRow("valid_max_half")
      << Bigint::fromInt(std::numeric_limits<Tint>::max() / 2)
      << true
      << Tint(std::numeric_limits<Tint>::max() / 2);

  QTest::newRow("overflow_min-1")
      << (Bigint::fromInt(std::numeric_limits<Tint>::min()) - Bigint(1))
      << false
      << Tint(0);

  QTest::newRow("overflow_max+1")
      << (Bigint::fromInt(std::numeric_limits<Tint>::max()) + Bigint(1))
      << false
      << Tint(0);
}

// Body for test function, that checks integer in/out handling
template <typename Tint>
void integertest()
{
  QFETCH(Bigint, source);
  QFETCH(bool, expected_ok);
  QFETCH(Tint, expected_result);

  bool actual_ok = false;
  Tint actual_result = bigint2int<Tint>(source, &actual_ok);

  QCOMPARE(actual_ok, expected_ok);
  QCOMPARE(actual_result, expected_result);
}

class ut_bigint: public QObject
{
  Q_OBJECT



  void int_conversions_uint16()
  {
    QFETCH(Bigint, source);
    QFETCH(bool, expected_ok);
    QFETCH(qint16, expected_result);

    bool actual_ok = false;
    qint16 actual_result = source.toInt16(&actual_ok);

    QCOMPARE(actual_ok, expected_ok);
    QCOMPARE(actual_result, expected_result);
  }

private slots:

  // This is a test copied from mbedtls_mpi_self_test
  void selfTest()
  {
    Bigint A{
        "EFE021C2645FD1DC586E69184AF4A31E" \
        "D5F53E93B5F123FA41680867BA110131" \
        "944FE7952E2517337780CB0DB80E61AA" \
        "E7C8DDC6C5C6AADEB34EB38A2F40D5E6", 16};

    Bigint N{
        "0066A198186C18C10B2F5ED9B522752A" \
        "9830B69916E535C8F047518A889A43A5" \
        "94B6BED27A168D31D4A52F88925AA8F5", 16};

    Bigint X = A * N;

    Bigint U{
        "602AB7ECA597A3D6B56FF9829A5E8B85" \
        "9E857EA95A03512E2BAE7391688D264A" \
        "A5663B0341DB9CCFD2C4C5F421FEC814" \
        "8001B72E848A38CAE1C65F78E56ABDEF" \
        "E12D3C039B8A02D6BE593F0BBBDA56F1" \
        "ECF677152EF804370C1A305CAF3B5BF1" \
        "30879B56C61DE584A0F53A2447A51E", 16};

    QVERIFY2(X == U, "MPI test #1 (mul_mpi)");

    std::pair<Bigint, Bigint> XY = A.div(N);

    U = Bigint{
        "256567336059E52CAE22925474705F39A94", 16};

    Bigint V{
        "6613F26162223DF488E9CD48CC132C7A" \
        "0AC93C701B001B092E4E5B9F73BCD27B" \
        "9EE50D0657C77F374E903CDFA4C642", 16};

    QVERIFY2(XY.first == U, "MPI test #2 (div_mpi)");
    QVERIFY2(XY.second == V, "MPI test #2 (div_mpi)");

#ifdef FULL_BIGINT_LIB
    Bigint E{
        "B2E7EFD37075B9F03FF989C7C5051C20" \
        "34D2A323810251127E7BF8625A4F49A5" \
        "F3E27F4DA8BD59C47D6DAABA4C8127BD" \
        "5B5C25763222FEFCCFC38B832366C29E", 16};

    X = A.expMod(E, N);

    U = Bigint{
        "36E139AEA55215609D2816998ED020BB" \
        "BD96C37890F65171D948E9BC7CBAA4D9" \
        "325D24D6A3C12710F10A09FA08AB87", 16};

    QVERIFY2(X == U, "MPI test #3 (exp_mod)");

    X = A.invMod(N);

    U = Bigint{
        "003A0AAEDD7E784FC07D8F9EC6E3BFD5" \
        "C3DBA76456363A10869622EAC2DD84EC" \
        "C5B8A74DAC4D09E03B5E0BE779F2DF61", 16};

    QVERIFY2(X == U, "MPI test #4 (inv_mod)");

    for( int i = 0; i < GCD_PAIR_COUNT; i++ )
    {
        Bigint X = Bigint(gcd_pairs[i][0]);
        Bigint Y = Bigint(gcd_pairs[i][1]);

        Bigint A = X.gcd(Y);

        QVERIFY2(A == Bigint(gcd_pairs[i][2]), "MPI test #5 (simple gcd)");
    }
#endif
  }

  // Checks different constructors
  void can_construct()
  {
    Bigint A; // Empty object
    QVERIFY(A == Bigint());
    QVERIFY(A == Bigint(bigint_sint_t(0)));

    Bigint B{54321}; // From number
    QVERIFY(B == Bigint(54321));

    Bigint C{"54321"}; // From const char*
    QVERIFY(C == Bigint(54321));

    Bigint D{std::string("54321")};
    QVERIFY(D == Bigint(54321));

    Bigint E{QStringLiteral("54321")};
    QVERIFY(D == Bigint(54321));

    QBitArray bitArray(10, true);
    bitArray.setBit(9, false);
    bitArray.setBit(1, false);
    Bigint F(bitArray);
    QVERIFY(F == Bigint(509));
  }

  // Check "isEmpty"
  void can_check_for_emptiness()
  {
    Bigint A;
    QVERIFY(A.isEmpty());

    Bigint B{123};
    QVERIFY(!B.isEmpty());

    Bigint C{bigint_sint_t(0)};
    QVERIFY(C.isEmpty());
  }

  void can_binary_encode_data()
  {
    QTest::addColumn<Bigint>("test");

    QTest::newRow("empty") << Bigint();
    QTest::newRow("123") << Bigint(123);
    QTest::newRow("longInt") << Bigint{
                                "602AB7ECA597A3D6B56FF9829A5E8B85" \
                                "9E857EA95A03512E2BAE7391688D264A" \
                                "A5663B0341DB9CCFD2C4C5F421FEC814" \
                                "8001B72E848A38CAE1C65F78E56ABDEF" \
                                "E12D3C039B8A02D6BE593F0BBBDA56F1" \
                                "ECF677152EF804370C1A305CAF3B5BF1" \
                                "30879B56C61DE584A0F53A2447A51E", 16};
  }

  // check to/from binary
  void can_binary_encode()
  {
    QFETCH(Bigint, test);

    std::string encodedAsString = test.toBinary();
    Bigint decodedFromString = Bigint::fromBinary(encodedAsString);
    QCOMPARE(decodedFromString, test);

    QByteArray encodedAsByteArray = test.toQByteArray();
    Bigint decodedFromByteArray = Bigint::fromQByteArray(encodedAsByteArray);
    QCOMPARE(decodedFromByteArray, test);
  }

  // check to/from text
  void can_text_encode()
  {
    QCOMPARE(Bigint().toString(), std::string("0"));
    QCOMPARE(Bigint(123).toString(), std::string("123"));
    QCOMPARE(Bigint("DEADBEEF", 16).toString(16), std::string("DEADBEEF"));

    QCOMPARE(Bigint().toQString(), QStringLiteral("0"));
    QCOMPARE(Bigint(123).toQString(), QStringLiteral("123"));
    QCOMPARE(Bigint("DEADBEEF", 16).toQString(16), QStringLiteral("DEADBEEF"));
  }

  // Check QBitArray
  void can_work_with_bit_array()
  {
    const int size = 512;
    QBitArray expected(size, true);

    for (int n = 0; n < 10; ++n)
      expected.setBit(qrand() % (size - 1), false);

    QBitArray actual = Bigint(expected).toQBitArray();

    QCOMPARE(actual, expected);
  }

  // Check operators
  void is_operators_works()
  {
    Bigint A{1000};
    Bigint B{100};
    Bigint C{3};

    QCOMPARE(A * B, Bigint(100000));
    QCOMPARE(A * 100, Bigint(100000));
    QCOMPARE(1000 * B, Bigint(100000));
    QCOMPARE(Bigint(A)*=B, Bigint(100000));
    QCOMPARE(Bigint(A)*=100, Bigint(100000));

    QCOMPARE(A / C, Bigint(333));
    QCOMPARE(A / B, Bigint(10));
    QCOMPARE(A / 100, Bigint(10));
    QCOMPARE(1000 / B, Bigint(10));
    QCOMPARE(Bigint(A)/=B, Bigint(10));
    QCOMPARE(Bigint(A)/=100, Bigint(10));

    QCOMPARE(A % C, Bigint(1));
    QCOMPARE(A % 3, 1);
    QCOMPARE(1000 % C, Bigint(1));
    QCOMPARE(Bigint(A)%=C, Bigint(1));
    QCOMPARE(Bigint(A)%=3, Bigint(1));

    QCOMPARE(A + B, Bigint(1100));
    QCOMPARE(A + 100, Bigint(1100));
    QCOMPARE(1000 + B, Bigint(1100));
    QCOMPARE(Bigint(A)+=B, Bigint(1100));
    QCOMPARE(Bigint(A)+=100, Bigint(1100));

    Bigint D = C;
    Bigint E = ++D;
    QCOMPARE(D, Bigint(4));
    QCOMPARE(E, Bigint(4));
    Bigint F = D++;
    QCOMPARE(D, Bigint(5));
    QCOMPARE(F, Bigint(4));

    QCOMPARE(A - B, Bigint(900));
    QCOMPARE(A - 100, Bigint(900));
    QCOMPARE(1000 - B, Bigint(900));
    QCOMPARE(Bigint(A)-=B, Bigint(900));
    QCOMPARE(Bigint(A)-=100, Bigint(900));

    D = C;
    E = --D;
    QCOMPARE(D, Bigint(2));
    QCOMPARE(E, Bigint(2));
    F = D--;
    QCOMPARE(D, Bigint(1));
    QCOMPARE(F, Bigint(2));

    QCOMPARE(A << 1, Bigint(2000));
    QCOMPARE(Bigint(A)<<=1, Bigint(2000));

    QCOMPARE(A >> 1, Bigint(500));
    QCOMPARE(Bigint(A)>>=1, Bigint(500));

    // unary
    QCOMPARE(-A, Bigint(-1000));
    QCOMPARE(~A, Bigint(~1000 & 1023)); // Only 10 bits are inverted!!!

    // bool casting
    QCOMPARE(!Bigint(), true);
    QCOMPARE(!Bigint(1), false);
  }

  void int16_io_data()
  {
    integertest_data<qint16>();
  }

  void int16_io()
  {
    integertest<qint16>();
  }

  void uint16_io_data()
  {
    integertest_data<quint16>();
  }

  void uint16_io()
  {
    integertest<quint16>();
  }

  void int32_io_data()
  {
    integertest_data<qint32>();
  }

  void int32_io()
  {
    integertest<qint32>();
  }

  void uint32_io_data()
  {
    integertest_data<quint32>();
  }

  void uint32_io()
  {
    integertest<quint32>();
  }

  void int64_io_data()
  {
    integertest_data<qint64>();
  }

  void int64_io()
  {
    integertest<qint64>();
  }

  void uint64_io_data()
  {
    integertest_data<quint64>();
  }

  void uint64_io()
  {
    integertest<quint64>();
  }
};

QTEST_MAIN(ut_bigint)

#include "ut_bigint.moc"
