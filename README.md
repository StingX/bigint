# BigInt

BigInt is a simple and relatively lightweight C++ large integers library. It consists
of the single C++ class header and one implementation file. The library has no external
dependencies except STL. 

If `Bigint` compiled with a Qt library, it provides some handy functions: 
QDebug output operator, QDataStream operators, QVariant cast operators and more.

The library tries to use inline ASM for some platforms:

* Intel x32/x64; PowerPC x32/x64; MC68000; Microblaze; Tricore on GNU compilers
* Intel x32 assembly on MSVC compiler

The original code came from [mbedtls mpi](https://github.com/ARMmbed/mbedtls)
 
# Usage

The usage is very simple:

	#include "Bigint.h"
	...
	Bigint test = 123 * Bigint("123456789123456789");
	auto result = test.div(5567);
	qDebug() << test << "/ 5567 =" << result.first;
	qDebug() << test << "% 5567 =" << result.second;
	
# Installation

Just copy n paste `Bigint.cpp` and `Bigint.h` files into your project. 

If you are creating some DLL in MSVC and want to export the symbol `Bigint`,
then import it into other modules, you can define `BIGINT_EXPORT` as 
`declspec(dllimport)`/`declspec(dllexport)` when appropriate. 
In Qt, you can do this with `Q_DECL_EXPORT`/`Q_DECL_IMPORT`.