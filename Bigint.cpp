/** \file Bigint.cpp
  * \brief Bigint large numbers class implementation
  * \author Devyatnikov A.
  * \date 04.06.2016
  * Project: Bigint
  *
  * Licensed under the Apache License, Version 2.0 (the "License"); you may
  *  not use this file except in compliance with the License.
  *  You may obtain a copy of the License at
  *
  *  http://www.apache.org/licenses/LICENSE-2.0
  *
  *  Unless required by applicable law or agreed to in writing, software
  *  distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
  *  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  *  See the License for the specific language governing permissions and
  *  limitations under the License.
  *
  *  This implementation based on https://tls.mbed.org
  */

#include "Bigint.h"

#include <cstddef>
#include <cstring>
#include <cstdio>
#include <cstdlib>
#include <QBitArray>

/*****************************************************************************
 * The content of mbedtls "bignum.h"
 *****************************************************************************/

#define BIGINT_ERR_MPI_FILE_IO_ERROR                     -0x0002  /**< An error occurred while reading from or writing to a file. */
#define BIGINT_ERR_MPI_BAD_INPUT_DATA                    -0x0004  /**< Bad input parameters to function. */
#define BIGINT_ERR_MPI_INVALID_CHARACTER                 -0x0006  /**< There is an invalid character in the digit string. */
#define BIGINT_ERR_MPI_BUFFER_TOO_SMALL                  -0x0008  /**< The buffer is too small to write to. */
#define BIGINT_ERR_MPI_NEGATIVE_VALUE                    -0x000A  /**< The input arguments are negative or result in illegal output. */
#define BIGINT_ERR_MPI_DIVISION_BY_ZERO                  -0x000C  /**< The input argument for division is zero, which is not allowed. */
#define BIGINT_ERR_MPI_NOT_ACCEPTABLE                    -0x000E  /**< The input arguments are not acceptable. */
#define BIGINT_ERR_MPI_ALLOC_FAILED                      -0x0010  /**< Memory allocation failed. */

#define BIGINT_MPI_CHK(f) do { if( ( ret = f ) != 0 ) goto cleanup; } while( 0 )

struct Bigint::mbedtls
{

  /**
   * \brief           Initialize one MPI (make internal references valid)
   *                  This just makes it ready to be set or freed,
   *                  but does not define a value for the MPI.
   *
   * \param X         One MPI to initialize.
   */
  static void mpi_init( Bigint::Data *X );

  /**
   * \brief          Unallocate one MPI
   *
   * \param X        One MPI to unallocate.
   */
  static void mpi_free( Bigint::Data *X );

  /**
   * \brief          Enlarge to the specified number of limbs
   *
   * \param X        MPI to grow
   * \param nblimbs  The target number of limbs
   *
   * \return         0 if successful,
   *                 BIGINT_ERR_MPI_ALLOC_FAILED if memory allocation failed
   */
  static int mpi_grow( Bigint::Data *X, size_t nblimbs );

  /**
   * \brief          Resize down, keeping at least the specified number of limbs
   *
   * \param X        MPI to shrink
   * \param nblimbs  The minimum number of limbs to keep
   *
   * \return         0 if successful,
   *                 BIGINT_ERR_MPI_ALLOC_FAILED if memory allocation failed
   */
  static int mpi_shrink( Bigint::Data *X, size_t nblimbs );

  /**
   * \brief          Copy the contents of Y into X
   *
   * \param X        Destination MPI
   * \param Y        Source MPI
   *
   * \return         0 if successful,
   *                 BIGINT_ERR_MPI_ALLOC_FAILED if memory allocation failed
   */
  static int mpi_copy( Bigint::Data *X, const Bigint::Data *Y );

  /**
   * \brief          Swap the contents of X and Y
   *
   * \param X        First MPI value
   * \param Y        Second MPI value
   */
  void mpi_swap( Bigint::Data *X, Bigint::Data *Y );

  #ifdef FULL_BIGINT_LIB
  /**
   * \brief          Safe conditional assignement X = Y if assign is 1
   *
   * \param X        MPI to conditionally assign to
   * \param Y        Value to be assigned
   * \param assign   1: perform the assignment, 0: keep X's original value
   *
   * \return         0 if successful,
   *                 BIGINT_ERR_MPI_ALLOC_FAILED if memory allocation failed,
   *
   * \note           This function is equivalent to
   *                      if( assign ) mpi_copy( X, Y );
   *                 except that it avoids leaking any information about whether
   *                 the assignment was done or not (the above code may leak
   *                 information through branch prediction and/or memory access
   *                 patterns analysis).
   */
  static int mpi_safe_cond_assign( Bigint::Data *X, const Bigint::Data *Y, unsigned char assign );

  /**
   * \brief          Safe conditional swap X <-> Y if swap is 1
   *
   * \param X        First mbedtls_mpi value
   * \param Y        Second mbedtls_mpi value
   * \param assign   1: perform the swap, 0: keep X and Y's original values
   *
   * \return         0 if successful,
   *                 BIGINT_ERR_MPI_ALLOC_FAILED if memory allocation failed,
   *
   * \note           This function is equivalent to
   *                      if( assign ) mpi_swap( X, Y );
   *                 except that it avoids leaking any information about whether
   *                 the assignment was done or not (the above code may leak
   *                 information through branch prediction and/or memory access
   *                 patterns analysis).
   */
  static int mpi_safe_cond_swap( Bigint::Data *X, Bigint::Data *Y, unsigned char assign );
  #endif

  /**
   * \brief          Set value from integer
   *
   * \param X        MPI to set
   * \param z        Value to use
   *
   * \return         0 if successful,
   *                 BIGINT_ERR_MPI_ALLOC_FAILED if memory allocation failed
   */
  static int mpi_lset( Bigint::Data *X, bigint_sint_t z );

  /**
   * \brief          Get a specific bit from X
   *
   * \param X        MPI to use
   * \param pos      Zero-based index of the bit in X
   *
   * \return         Either a 0 or a 1
   */
  static int mpi_get_bit( const Bigint::Data *X, size_t pos );

  /**
   * \brief          Set a bit of X to a specific value of 0 or 1
   *
   * \note           Will grow X if necessary to set a bit to 1 in a not yet
   *                 existing limb. Will not grow if bit should be set to 0
   *
   * \param X        MPI to use
   * \param pos      Zero-based index of the bit in X
   * \param val      The value to set the bit to (0 or 1)
   *
   * \return         0 if successful,
   *                 BIGINT_ERR_MPI_ALLOC_FAILED if memory allocation failed,
   *                 BIGINT_ERR_MPI_BAD_INPUT_DATA if val is not 0 or 1
   */
  static int mpi_set_bit( Bigint::Data *X, size_t pos, unsigned char val );

  /**
   * \brief          Return the number of zero-bits before the least significant
   *                 '1' bit
   *
   * Note: Thus also the zero-based index of the least significant '1' bit
   *
   * \param X        MPI to use
   */
  static size_t mpi_lsb( const Bigint::Data *X );

  /**
   * \brief          Return the number of bits up to and including the most
   *                 significant '1' bit'
   *
   * Note: Thus also the one-based index of the most significant '1' bit
   *
   * \param X        MPI to use
   */
  static size_t mpi_bitlen( const Bigint::Data *X );

  /**
   * \brief          Return the total size in bytes
   *
   * \param X        MPI to use
   */
  static size_t mpi_size( const Bigint::Data *X );

  /**
   * \brief          Import from an ASCII string
   *
   * \param X        Destination MPI
   * \param radix    Input numeric base
   * \param s        string buffer
   * \param slen     length of the string buffer
   *
   * \return         0 if successful, or a BIGINT_ERR_MPI_XXX error code
   */
  template <typename Tchar>
  static int mpi_read_string( Bigint::Data *X, int radix, const Tchar *s, size_t slen);

  template <typename Tchar>
  static int mpi_write_hlp( Bigint::Data *X, int radix, Tchar **p );

  /**
   * \brief          Export into an ASCII string
   *
   * \param X        Source MPI
   * \param radix    Output numeric base
   * \param buf      Buffer to write the string to
   * \param buflen   Length of buf
   * \param olen     Length of the string written, including final NUL byte
   *
   * \return         0 if successful, or a BIGINT_ERR_MPI_XXX error code.
   *                 *olen is always updated to reflect the amount
   *                 of data that has (or would have) been written.
   *
   * \note           Call this function with buflen = 0 to obtain the
   *                 minimum required buffer size in *olen.
   */
  template <typename Tchar>
  static int mpi_write_string( const Bigint::Data *X, int radix,
                                Tchar *buf, size_t buflen, size_t *olen );

  /**
   * \brief          Import X from unsigned binary data, big endian
   *
   * \param X        Destination MPI
   * \param buf      Input buffer
   * \param buflen   Input buffer size
   *
   * \return         0 if successful,
   *                 BIGINT_ERR_MPI_ALLOC_FAILED if memory allocation failed
   */
  static int mpi_read_binary( Bigint::Data *X, const unsigned char *buf, size_t buflen );

  /**
   * \brief          Export X into unsigned binary data, big endian.
   *                 Always fills the whole buffer, which will start with zeros
   *                 if the number is smaller.
   *
   * \param X        Source MPI
   * \param buf      Output buffer
   * \param buflen   Output buffer size
   *
   * \return         0 if successful,
   *                 BIGINT_ERR_MPI_BUFFER_TOO_SMALL if buf isn't large enough
   */
  static int mpi_write_binary( const Bigint::Data *X, unsigned char *buf, size_t buflen );

  /**
   * \brief          Left-shift: X <<= count
   *
   * \param X        MPI to shift
   * \param count    Amount to shift
   *
   * \return         0 if successful,
   *                 BIGINT_ERR_MPI_ALLOC_FAILED if memory allocation failed
   */
  static int mpi_shift_l( Bigint::Data *X, size_t count );

  /**
   * \brief          Right-shift: X >>= count
   *
   * \param X        MPI to shift
   * \param count    Amount to shift
   *
   * \return         0 if successful,
   *                 BIGINT_ERR_MPI_ALLOC_FAILED if memory allocation failed
   */
  static int mpi_shift_r( Bigint::Data *X, size_t count );

  /**
   * \brief          Compare unsigned values
   *
   * \param X        Left-hand MPI
   * \param Y        Right-hand MPI
   *
   * \return         1 if |X| is greater than |Y|,
   *                -1 if |X| is lesser  than |Y| or
   *                 0 if |X| is equal to |Y|
   */
  static int mpi_cmp_abs( const Bigint::Data *X, const Bigint::Data *Y );

  /**
   * \brief          Compare signed values
   *
   * \param X        Left-hand MPI
   * \param Y        Right-hand MPI
   *
   * \return         1 if X is greater than Y,
   *                -1 if X is lesser  than Y or
   *                 0 if X is equal to Y
   */
  static int mpi_cmp_mpi( const Bigint::Data *X, const Bigint::Data *Y );

  /**
   * \brief          Compare signed values
   *
   * \param X        Left-hand MPI
   * \param z        The integer value to compare to
   *
   * \return         1 if X is greater than z,
   *                -1 if X is lesser  than z or
   *                 0 if X is equal to z
   */
  static int mpi_cmp_int( const Bigint::Data *X, bigint_sint_t z );

  /**
   * \brief          Unsigned addition: X = |A| + |B|
   *
   * \param X        Destination MPI
   * \param A        Left-hand MPI
   * \param B        Right-hand MPI
   *
   * \return         0 if successful,
   *                 BIGINT_ERR_MPI_ALLOC_FAILED if memory allocation failed
   */
  static int mpi_add_abs( Bigint::Data *X, const Bigint::Data *A, const Bigint::Data *B );

  /**
   * \brief          Unsigned subtraction: X = |A| - |B|
   *
   * \param X        Destination MPI
   * \param A        Left-hand MPI
   * \param B        Right-hand MPI
   *
   * \return         0 if successful,
   *                 BIGINT_ERR_MPI_NEGATIVE_VALUE if B is greater than A
   */
  static int mpi_sub_abs( Bigint::Data *X, const Bigint::Data *A, const Bigint::Data *B );

  /**
   * \brief          Signed addition: X = A + B
   *
   * \param X        Destination MPI
   * \param A        Left-hand MPI
   * \param B        Right-hand MPI
   *
   * \return         0 if successful,
   *                 BIGINT_ERR_MPI_ALLOC_FAILED if memory allocation failed
   */
  static int mpi_add_mpi( Bigint::Data *X, const Bigint::Data *A, const Bigint::Data *B );

  /**
   * \brief          Signed subtraction: X = A - B
   *
   * \param X        Destination MPI
   * \param A        Left-hand MPI
   * \param B        Right-hand MPI
   *
   * \return         0 if successful,
   *                 BIGINT_ERR_MPI_ALLOC_FAILED if memory allocation failed
   */
  static int mpi_sub_mpi( Bigint::Data *X, const Bigint::Data *A, const Bigint::Data *B );

  /**
   * \brief          Signed addition: X = A + b
   *
   * \param X        Destination MPI
   * \param A        Left-hand MPI
   * \param b        The integer value to add
   *
   * \return         0 if successful,
   *                 BIGINT_ERR_MPI_ALLOC_FAILED if memory allocation failed
   */
  static int mpi_add_int( Bigint::Data *X, const Bigint::Data *A, bigint_sint_t b );

  /**
   * \brief          Signed subtraction: X = A - b
   *
   * \param X        Destination MPI
   * \param A        Left-hand MPI
   * \param b        The integer value to subtract
   *
   * \return         0 if successful,
   *                 BIGINT_ERR_MPI_ALLOC_FAILED if memory allocation failed
   */
  static int mpi_sub_int( Bigint::Data *X, const Bigint::Data *A, bigint_sint_t b );

  /**
   * \brief          Baseline multiplication: X = A * B
   *
   * \param X        Destination MPI
   * \param A        Left-hand MPI
   * \param B        Right-hand MPI
   *
   * \return         0 if successful,
   *                 BIGINT_ERR_MPI_ALLOC_FAILED if memory allocation failed
   */
  static int mpi_mul_mpi( Bigint::Data *X, const Bigint::Data *A, const Bigint::Data *B );

  /**
   * \brief          Baseline multiplication: X = A * b
   *
   * \param X        Destination MPI
   * \param A        Left-hand MPI
   * \param b        The unsigned integer value to multiply with
   *
   * \note           b is unsigned
   *
   * \return         0 if successful,
   *                 BIGINT_ERR_MPI_ALLOC_FAILED if memory allocation failed
   */
  static int mpi_mul_int( Bigint::Data *X, const Bigint::Data *A, bigint_uint_t b );

  /**
   * \brief          Division by mbedtls_mpi: A = Q * B + R
   *
   * \param Q        Destination MPI for the quotient
   * \param R        Destination MPI for the rest value
   * \param A        Left-hand MPI
   * \param B        Right-hand MPI
   *
   * \return         0 if successful,
   *                 BIGINT_ERR_MPI_ALLOC_FAILED if memory allocation failed,
   *                 BIGINT_ERR_MPI_DIVISION_BY_ZERO if B == 0
   *
   * \note           Either Q or R can be NULL.
   */
  static int mpi_div_mpi( Bigint::Data *Q, Bigint::Data *R, const Bigint::Data *A, const Bigint::Data *B );

  /**
   * \brief          Division by int: A = Q * b + R
   *
   * \param Q        Destination MPI for the quotient
   * \param R        Destination MPI for the rest value
   * \param A        Left-hand MPI
   * \param b        Integer to divide by
   *
   * \return         0 if successful,
   *                 BIGINT_ERR_MPI_ALLOC_FAILED if memory allocation failed,
   *                 BIGINT_ERR_MPI_DIVISION_BY_ZERO if b == 0
   *
   * \note           Either Q or R can be NULL.
   */
  static int mpi_div_int( Bigint::Data *Q, Bigint::Data *R, const Bigint::Data *A, bigint_sint_t b );

  /**
   * \brief          Modulo: R = A mod B
   *
   * \param R        Destination MPI for the rest value
   * \param A        Left-hand MPI
   * \param B        Right-hand MPI
   *
   * \return         0 if successful,
   *                 BIGINT_ERR_MPI_ALLOC_FAILED if memory allocation failed,
   *                 BIGINT_ERR_MPI_DIVISION_BY_ZERO if B == 0,
   *                 BIGINT_ERR_MPI_NEGATIVE_VALUE if B < 0
   */
  static int mpi_mod_mpi( Bigint::Data *R, const Bigint::Data *A, const Bigint::Data *B );

  /**
   * \brief          Modulo: r = A mod b
   *
   * \param r        Destination bigint_uint_t
   * \param A        Left-hand MPI
   * \param b        Integer to divide by
   *
   * \return         0 if successful,
   *                 BIGINT_ERR_MPI_ALLOC_FAILED if memory allocation failed,
   *                 BIGINT_ERR_MPI_DIVISION_BY_ZERO if b == 0,
   *                 BIGINT_ERR_MPI_NEGATIVE_VALUE if b < 0
   */
  static int mpi_mod_int( bigint_uint_t *r, const Bigint::Data *A, bigint_sint_t b );

#ifdef FULL_BIGINT_LIB

  // Inernal helpers
  static void mpi_montg_init( bigint_uint_t *mm, const Bigint::Data *N);
  static int mpi_montmul( Bigint::Data *A, const Bigint::Data *B, const Bigint::Data *N, bigint_uint_t mm, const Bigint::Data *T );
  static int mpi_montred( Bigint::Data *A, const Bigint::Data *N, bigint_uint_t mm, const Bigint::Data *T );

  /**
   * \brief          Sliding-window exponentiation: X = A^E mod N
   *
   * \param X        Destination MPI
   * \param A        Left-hand MPI
   * \param E        Exponent MPI
   * \param N        Modular MPI
   * \param _RR      Speed-up MPI used for recalculations
   *
   * \return         0 if successful,
   *                 BIGINT_ERR_MPI_ALLOC_FAILED if memory allocation failed,
   *                 BIGINT_ERR_MPI_BAD_INPUT_DATA if N is negative or even or
   *                 if E is negative
   *
   * \note           _RR is used to avoid re-computing R*R mod N across
   *                 multiple calls, which speeds up things a bit. It can
   *                 be set to NULL if the extra performance is unneeded.
   */
  static int mpi_exp_mod( Bigint::Data *X, const Bigint::Data *A, const Bigint::Data *E, const Bigint::Data *N, Bigint::Data *_RR );

  /**
   * \brief          Fill an MPI X with size bytes of random
   *
   * \param X        Destination MPI
   * \param size     Size in bytes
   * \param f_rng    RNG function
   * \param p_rng    RNG parameter
   *
   * \return         0 if successful,
   *                 BIGINT_ERR_MPI_ALLOC_FAILED if memory allocation failed
   */
  static int mpi_fill_random( Bigint::Data *X, size_t size,
                       int (*f_rng)(void *, unsigned char *, size_t),
                       void *p_rng );

  /**
   * \brief          Greatest common divisor: G = gcd(A, B)
   *
   * \param G        Destination MPI
   * \param A        Left-hand MPI
   * \param B        Right-hand MPI
   *
   * \return         0 if successful,
   *                 BIGINT_ERR_MPI_ALLOC_FAILED if memory allocation failed
   */
  static int mpi_gcd( Bigint::Data *G, const Bigint::Data *A, const Bigint::Data *B );

  /**
   * \brief          Modular inverse: X = A^-1 mod N
   *
   * \param X        Destination MPI
   * \param A        Left-hand MPI
   * \param N        Right-hand MPI
   *
   * \return         0 if successful,
   *                 BIGINT_ERR_MPI_ALLOC_FAILED if memory allocation failed,
   *                 BIGINT_ERR_MPI_BAD_INPUT_DATA if N is negative or nil
                     BIGINT_ERR_MPI_NOT_ACCEPTABLE if A has no inverse mod N
   */
  static int mpi_inv_mod( Bigint::Data *X, const Bigint::Data *A, const Bigint::Data *N );

  // Helpers
  static int mpi_check_small_factors( const Bigint::Data *X );
  static int mpi_miller_rabin( const Bigint::Data *X,
                               int (*f_rng)(void *, unsigned char *, size_t),
                               void *p_rng );

  /**
   * \brief          Miller-Rabin primality test
   *
   * \param X        MPI to check
   * \param f_rng    RNG function
   * \param p_rng    RNG parameter
   *
   * \return         0 if successful (probably prime),
   *                 BIGINT_ERR_MPI_ALLOC_FAILED if memory allocation failed,
   *                 BIGINT_ERR_MPI_NOT_ACCEPTABLE if X is not prime
   */
  static int mpi_is_prime( const Bigint::Data *X,
                    int (*f_rng)(void *, unsigned char *, size_t),
                    void *p_rng );

  /**
   * \brief          Prime number generation
   *
   * \param X        Destination MPI
   * \param nbits    Required size of X in bits
   *                 ( 3 <= nbits <= BIGINT_MPI_MAX_BITS )
   * \param dh_flag  If 1, then (X-1)/2 will be prime too
   * \param f_rng    RNG function
   * \param p_rng    RNG parameter
   *
   * \return         0 if successful (probably prime),
   *                 BIGINT_ERR_MPI_ALLOC_FAILED if memory allocation failed,
   *                 BIGINT_ERR_MPI_BAD_INPUT_DATA if nbits is < 3
   */
  static int mpi_gen_prime( Bigint::Data *X, size_t nbits, int dh_flag,
                     int (*f_rng)(void *, unsigned char *, size_t),
                     void *p_rng );

#endif

  template <typename Tint>
  static Tint extract_int(const Bigint::Data* x, bool* ok);

  template <typename Tint>
  static Bigint from_int(Tint v);

}; // end struct Bigint::mbedtls

/*****************************************************************************
 * The content of mbedtls "bn_mul.h"
 *****************************************************************************/

/*
 *      Multiply source vector [s] with b, add result
 *       to destination vector [d] and set carry c.
 *
 *      Currently supports:
 *
 *         . IA-32 (386+)         . AMD64 / EM64T
 *         . IA-32 (SSE2)         . Motorola 68000
 *         . PowerPC, 32-bit      . MicroBlaze
 *         . PowerPC, 64-bit      . TriCore
 *         . SPARC v8             . ARM v3+
 *         . Alpha                . MIPS32
 *         . C, longlong          . C, generic
 */

#if defined(BIGINT_HAVE_ASM)

#ifndef asm
# define asm __asm
#endif

/* armcc5 --gnu defines __GNUC__ but doesn't support GNU's extended asm */
#if defined(__GNUC__) && \
    ( !defined(__ARMCC_VERSION) || __ARMCC_VERSION >= 6000000 )
#if defined(__i386__)

#define MULADDC_INIT                        \
    asm(                                    \
        "movl   %%ebx, %0           \n\t"   \
        "movl   %5, %%esi           \n\t"   \
        "movl   %6, %%edi           \n\t"   \
        "movl   %7, %%ecx           \n\t"   \
        "movl   %8, %%ebx           \n\t"

#define MULADDC_CORE                        \
        "lodsl                      \n\t"   \
        "mull   %%ebx               \n\t"   \
        "addl   %%ecx,   %%eax      \n\t"   \
        "adcl   $0,      %%edx      \n\t"   \
        "addl   (%%edi), %%eax      \n\t"   \
        "adcl   $0,      %%edx      \n\t"   \
        "movl   %%edx,   %%ecx      \n\t"   \
        "stosl                      \n\t"

#if defined(BIGINT_HAVE_SSE2)

#define MULADDC_HUIT                            \
        "movd     %%ecx,     %%mm1      \n\t"   \
        "movd     %%ebx,     %%mm0      \n\t"   \
        "movd     (%%edi),   %%mm3      \n\t"   \
        "paddq    %%mm3,     %%mm1      \n\t"   \
        "movd     (%%esi),   %%mm2      \n\t"   \
        "pmuludq  %%mm0,     %%mm2      \n\t"   \
        "movd     4(%%esi),  %%mm4      \n\t"   \
        "pmuludq  %%mm0,     %%mm4      \n\t"   \
        "movd     8(%%esi),  %%mm6      \n\t"   \
        "pmuludq  %%mm0,     %%mm6      \n\t"   \
        "movd     12(%%esi), %%mm7      \n\t"   \
        "pmuludq  %%mm0,     %%mm7      \n\t"   \
        "paddq    %%mm2,     %%mm1      \n\t"   \
        "movd     4(%%edi),  %%mm3      \n\t"   \
        "paddq    %%mm4,     %%mm3      \n\t"   \
        "movd     8(%%edi),  %%mm5      \n\t"   \
        "paddq    %%mm6,     %%mm5      \n\t"   \
        "movd     12(%%edi), %%mm4      \n\t"   \
        "paddq    %%mm4,     %%mm7      \n\t"   \
        "movd     %%mm1,     (%%edi)    \n\t"   \
        "movd     16(%%esi), %%mm2      \n\t"   \
        "pmuludq  %%mm0,     %%mm2      \n\t"   \
        "psrlq    $32,       %%mm1      \n\t"   \
        "movd     20(%%esi), %%mm4      \n\t"   \
        "pmuludq  %%mm0,     %%mm4      \n\t"   \
        "paddq    %%mm3,     %%mm1      \n\t"   \
        "movd     24(%%esi), %%mm6      \n\t"   \
        "pmuludq  %%mm0,     %%mm6      \n\t"   \
        "movd     %%mm1,     4(%%edi)   \n\t"   \
        "psrlq    $32,       %%mm1      \n\t"   \
        "movd     28(%%esi), %%mm3      \n\t"   \
        "pmuludq  %%mm0,     %%mm3      \n\t"   \
        "paddq    %%mm5,     %%mm1      \n\t"   \
        "movd     16(%%edi), %%mm5      \n\t"   \
        "paddq    %%mm5,     %%mm2      \n\t"   \
        "movd     %%mm1,     8(%%edi)   \n\t"   \
        "psrlq    $32,       %%mm1      \n\t"   \
        "paddq    %%mm7,     %%mm1      \n\t"   \
        "movd     20(%%edi), %%mm5      \n\t"   \
        "paddq    %%mm5,     %%mm4      \n\t"   \
        "movd     %%mm1,     12(%%edi)  \n\t"   \
        "psrlq    $32,       %%mm1      \n\t"   \
        "paddq    %%mm2,     %%mm1      \n\t"   \
        "movd     24(%%edi), %%mm5      \n\t"   \
        "paddq    %%mm5,     %%mm6      \n\t"   \
        "movd     %%mm1,     16(%%edi)  \n\t"   \
        "psrlq    $32,       %%mm1      \n\t"   \
        "paddq    %%mm4,     %%mm1      \n\t"   \
        "movd     28(%%edi), %%mm5      \n\t"   \
        "paddq    %%mm5,     %%mm3      \n\t"   \
        "movd     %%mm1,     20(%%edi)  \n\t"   \
        "psrlq    $32,       %%mm1      \n\t"   \
        "paddq    %%mm6,     %%mm1      \n\t"   \
        "movd     %%mm1,     24(%%edi)  \n\t"   \
        "psrlq    $32,       %%mm1      \n\t"   \
        "paddq    %%mm3,     %%mm1      \n\t"   \
        "movd     %%mm1,     28(%%edi)  \n\t"   \
        "addl     $32,       %%edi      \n\t"   \
        "addl     $32,       %%esi      \n\t"   \
        "psrlq    $32,       %%mm1      \n\t"   \
        "movd     %%mm1,     %%ecx      \n\t"

#define MULADDC_STOP                    \
        "emms                   \n\t"   \
        "movl   %4, %%ebx       \n\t"   \
        "movl   %%ecx, %1       \n\t"   \
        "movl   %%edi, %2       \n\t"   \
        "movl   %%esi, %3       \n\t"   \
        : "=m" (t), "=m" (c), "=m" (d), "=m" (s)        \
        : "m" (t), "m" (s), "m" (d), "m" (c), "m" (b)   \
        : "eax", "ecx", "edx", "esi", "edi"             \
    );

#else

#define MULADDC_STOP                    \
        "movl   %4, %%ebx       \n\t"   \
        "movl   %%ecx, %1       \n\t"   \
        "movl   %%edi, %2       \n\t"   \
        "movl   %%esi, %3       \n\t"   \
        : "=m" (t), "=m" (c), "=m" (d), "=m" (s)        \
        : "m" (t), "m" (s), "m" (d), "m" (c), "m" (b)   \
        : "eax", "ecx", "edx", "esi", "edi"             \
    );
#endif /* SSE2 */
#endif /* i386 */

#if defined(__amd64__) || defined (__x86_64__)

#define MULADDC_INIT                        \
    asm(                                    \
        "xorq   %%r8, %%r8          \n\t"

#define MULADDC_CORE                        \
        "movq   (%%rsi), %%rax      \n\t"   \
        "mulq   %%rbx               \n\t"   \
        "addq   $8,      %%rsi      \n\t"   \
        "addq   %%rcx,   %%rax      \n\t"   \
        "movq   %%r8,    %%rcx      \n\t"   \
        "adcq   $0,      %%rdx      \n\t"   \
        "nop                        \n\t"   \
        "addq   %%rax,   (%%rdi)    \n\t"   \
        "adcq   %%rdx,   %%rcx      \n\t"   \
        "addq   $8,      %%rdi      \n\t"

#define MULADDC_STOP                        \
        : "+c" (c), "+D" (d), "+S" (s)      \
        : "b" (b)                           \
        : "rax", "rdx", "r8"                \
    );

#endif /* AMD64 */

#if defined(__mc68020__) || defined(__mcpu32__)

#define MULADDC_INIT                    \
    asm(                                \
        "movl   %3, %%a2        \n\t"   \
        "movl   %4, %%a3        \n\t"   \
        "movl   %5, %%d3        \n\t"   \
        "movl   %6, %%d2        \n\t"   \
        "moveq  #0, %%d0        \n\t"

#define MULADDC_CORE                    \
        "movel  %%a2@+, %%d1    \n\t"   \
        "mulul  %%d2, %%d4:%%d1 \n\t"   \
        "addl   %%d3, %%d1      \n\t"   \
        "addxl  %%d0, %%d4      \n\t"   \
        "moveq  #0,   %%d3      \n\t"   \
        "addl   %%d1, %%a3@+    \n\t"   \
        "addxl  %%d4, %%d3      \n\t"

#define MULADDC_STOP                    \
        "movl   %%d3, %0        \n\t"   \
        "movl   %%a3, %1        \n\t"   \
        "movl   %%a2, %2        \n\t"   \
        : "=m" (c), "=m" (d), "=m" (s)              \
        : "m" (s), "m" (d), "m" (c), "m" (b)        \
        : "d0", "d1", "d2", "d3", "d4", "a2", "a3"  \
    );

#define MULADDC_HUIT                        \
        "movel  %%a2@+,  %%d1       \n\t"   \
        "mulul  %%d2,    %%d4:%%d1  \n\t"   \
        "addxl  %%d3,    %%d1       \n\t"   \
        "addxl  %%d0,    %%d4       \n\t"   \
        "addl   %%d1,    %%a3@+     \n\t"   \
        "movel  %%a2@+,  %%d1       \n\t"   \
        "mulul  %%d2,    %%d3:%%d1  \n\t"   \
        "addxl  %%d4,    %%d1       \n\t"   \
        "addxl  %%d0,    %%d3       \n\t"   \
        "addl   %%d1,    %%a3@+     \n\t"   \
        "movel  %%a2@+,  %%d1       \n\t"   \
        "mulul  %%d2,    %%d4:%%d1  \n\t"   \
        "addxl  %%d3,    %%d1       \n\t"   \
        "addxl  %%d0,    %%d4       \n\t"   \
        "addl   %%d1,    %%a3@+     \n\t"   \
        "movel  %%a2@+,  %%d1       \n\t"   \
        "mulul  %%d2,    %%d3:%%d1  \n\t"   \
        "addxl  %%d4,    %%d1       \n\t"   \
        "addxl  %%d0,    %%d3       \n\t"   \
        "addl   %%d1,    %%a3@+     \n\t"   \
        "movel  %%a2@+,  %%d1       \n\t"   \
        "mulul  %%d2,    %%d4:%%d1  \n\t"   \
        "addxl  %%d3,    %%d1       \n\t"   \
        "addxl  %%d0,    %%d4       \n\t"   \
        "addl   %%d1,    %%a3@+     \n\t"   \
        "movel  %%a2@+,  %%d1       \n\t"   \
        "mulul  %%d2,    %%d3:%%d1  \n\t"   \
        "addxl  %%d4,    %%d1       \n\t"   \
        "addxl  %%d0,    %%d3       \n\t"   \
        "addl   %%d1,    %%a3@+     \n\t"   \
        "movel  %%a2@+,  %%d1       \n\t"   \
        "mulul  %%d2,    %%d4:%%d1  \n\t"   \
        "addxl  %%d3,    %%d1       \n\t"   \
        "addxl  %%d0,    %%d4       \n\t"   \
        "addl   %%d1,    %%a3@+     \n\t"   \
        "movel  %%a2@+,  %%d1       \n\t"   \
        "mulul  %%d2,    %%d3:%%d1  \n\t"   \
        "addxl  %%d4,    %%d1       \n\t"   \
        "addxl  %%d0,    %%d3       \n\t"   \
        "addl   %%d1,    %%a3@+     \n\t"   \
        "addxl  %%d0,    %%d3       \n\t"

#endif /* MC68000 */

#if defined(__powerpc64__) || defined(__ppc64__)

#if defined(__MACH__) && defined(__APPLE__)

#define MULADDC_INIT                        \
    asm(                                    \
        "ld     r3, %3              \n\t"   \
        "ld     r4, %4              \n\t"   \
        "ld     r5, %5              \n\t"   \
        "ld     r6, %6              \n\t"   \
        "addi   r3, r3, -8          \n\t"   \
        "addi   r4, r4, -8          \n\t"   \
        "addic  r5, r5,  0          \n\t"

#define MULADDC_CORE                        \
        "ldu    r7, 8(r3)           \n\t"   \
        "mulld  r8, r7, r6          \n\t"   \
        "mulhdu r9, r7, r6          \n\t"   \
        "adde   r8, r8, r5          \n\t"   \
        "ld     r7, 8(r4)           \n\t"   \
        "addze  r5, r9              \n\t"   \
        "addc   r8, r8, r7          \n\t"   \
        "stdu   r8, 8(r4)           \n\t"

#define MULADDC_STOP                        \
        "addze  r5, r5              \n\t"   \
        "addi   r4, r4, 8           \n\t"   \
        "addi   r3, r3, 8           \n\t"   \
        "std    r5, %0              \n\t"   \
        "std    r4, %1              \n\t"   \
        "std    r3, %2              \n\t"   \
        : "=m" (c), "=m" (d), "=m" (s)              \
        : "m" (s), "m" (d), "m" (c), "m" (b)        \
        : "r3", "r4", "r5", "r6", "r7", "r8", "r9"  \
    );


#else /* __MACH__ && __APPLE__ */

#define MULADDC_INIT                        \
    asm(                                    \
        "ld     %%r3, %3            \n\t"   \
        "ld     %%r4, %4            \n\t"   \
        "ld     %%r5, %5            \n\t"   \
        "ld     %%r6, %6            \n\t"   \
        "addi   %%r3, %%r3, -8      \n\t"   \
        "addi   %%r4, %%r4, -8      \n\t"   \
        "addic  %%r5, %%r5,  0      \n\t"

#define MULADDC_CORE                        \
        "ldu    %%r7, 8(%%r3)       \n\t"   \
        "mulld  %%r8, %%r7, %%r6    \n\t"   \
        "mulhdu %%r9, %%r7, %%r6    \n\t"   \
        "adde   %%r8, %%r8, %%r5    \n\t"   \
        "ld     %%r7, 8(%%r4)       \n\t"   \
        "addze  %%r5, %%r9          \n\t"   \
        "addc   %%r8, %%r8, %%r7    \n\t"   \
        "stdu   %%r8, 8(%%r4)       \n\t"

#define MULADDC_STOP                        \
        "addze  %%r5, %%r5          \n\t"   \
        "addi   %%r4, %%r4, 8       \n\t"   \
        "addi   %%r3, %%r3, 8       \n\t"   \
        "std    %%r5, %0            \n\t"   \
        "std    %%r4, %1            \n\t"   \
        "std    %%r3, %2            \n\t"   \
        : "=m" (c), "=m" (d), "=m" (s)              \
        : "m" (s), "m" (d), "m" (c), "m" (b)        \
        : "r3", "r4", "r5", "r6", "r7", "r8", "r9"  \
    );

#endif /* __MACH__ && __APPLE__ */

#elif defined(__powerpc__) || defined(__ppc__) /* end PPC64/begin PPC32  */

#if defined(__MACH__) && defined(__APPLE__)

#define MULADDC_INIT                    \
    asm(                                \
        "lwz    r3, %3          \n\t"   \
        "lwz    r4, %4          \n\t"   \
        "lwz    r5, %5          \n\t"   \
        "lwz    r6, %6          \n\t"   \
        "addi   r3, r3, -4      \n\t"   \
        "addi   r4, r4, -4      \n\t"   \
        "addic  r5, r5,  0      \n\t"

#define MULADDC_CORE                    \
        "lwzu   r7, 4(r3)       \n\t"   \
        "mullw  r8, r7, r6      \n\t"   \
        "mulhwu r9, r7, r6      \n\t"   \
        "adde   r8, r8, r5      \n\t"   \
        "lwz    r7, 4(r4)       \n\t"   \
        "addze  r5, r9          \n\t"   \
        "addc   r8, r8, r7      \n\t"   \
        "stwu   r8, 4(r4)       \n\t"

#define MULADDC_STOP                    \
        "addze  r5, r5          \n\t"   \
        "addi   r4, r4, 4       \n\t"   \
        "addi   r3, r3, 4       \n\t"   \
        "stw    r5, %0          \n\t"   \
        "stw    r4, %1          \n\t"   \
        "stw    r3, %2          \n\t"   \
        : "=m" (c), "=m" (d), "=m" (s)              \
        : "m" (s), "m" (d), "m" (c), "m" (b)        \
        : "r3", "r4", "r5", "r6", "r7", "r8", "r9"  \
    );

#else /* __MACH__ && __APPLE__ */

#define MULADDC_INIT                        \
    asm(                                    \
        "lwz    %%r3, %3            \n\t"   \
        "lwz    %%r4, %4            \n\t"   \
        "lwz    %%r5, %5            \n\t"   \
        "lwz    %%r6, %6            \n\t"   \
        "addi   %%r3, %%r3, -4      \n\t"   \
        "addi   %%r4, %%r4, -4      \n\t"   \
        "addic  %%r5, %%r5,  0      \n\t"

#define MULADDC_CORE                        \
        "lwzu   %%r7, 4(%%r3)       \n\t"   \
        "mullw  %%r8, %%r7, %%r6    \n\t"   \
        "mulhwu %%r9, %%r7, %%r6    \n\t"   \
        "adde   %%r8, %%r8, %%r5    \n\t"   \
        "lwz    %%r7, 4(%%r4)       \n\t"   \
        "addze  %%r5, %%r9          \n\t"   \
        "addc   %%r8, %%r8, %%r7    \n\t"   \
        "stwu   %%r8, 4(%%r4)       \n\t"

#define MULADDC_STOP                        \
        "addze  %%r5, %%r5          \n\t"   \
        "addi   %%r4, %%r4, 4       \n\t"   \
        "addi   %%r3, %%r3, 4       \n\t"   \
        "stw    %%r5, %0            \n\t"   \
        "stw    %%r4, %1            \n\t"   \
        "stw    %%r3, %2            \n\t"   \
        : "=m" (c), "=m" (d), "=m" (s)              \
        : "m" (s), "m" (d), "m" (c), "m" (b)        \
        : "r3", "r4", "r5", "r6", "r7", "r8", "r9"  \
    );

#endif /* __MACH__ && __APPLE__ */

#endif /* PPC32 */

/*
 * The Sparc(64) assembly is reported to be broken.
 * Disable it for now, until we're able to fix it.
 */
#if 0 && defined(__sparc__)
#if defined(__sparc64__)

#define MULADDC_INIT                                    \
    asm(                                                \
                "ldx     %3, %%o0               \n\t"   \
                "ldx     %4, %%o1               \n\t"   \
                "ld      %5, %%o2               \n\t"   \
                "ld      %6, %%o3               \n\t"

#define MULADDC_CORE                                    \
                "ld      [%%o0], %%o4           \n\t"   \
                "inc     4, %%o0                \n\t"   \
                "ld      [%%o1], %%o5           \n\t"   \
                "umul    %%o3, %%o4, %%o4       \n\t"   \
                "addcc   %%o4, %%o2, %%o4       \n\t"   \
                "rd      %%y, %%g1              \n\t"   \
                "addx    %%g1, 0, %%g1          \n\t"   \
                "addcc   %%o4, %%o5, %%o4       \n\t"   \
                "st      %%o4, [%%o1]           \n\t"   \
                "addx    %%g1, 0, %%o2          \n\t"   \
                "inc     4, %%o1                \n\t"

        #define MULADDC_STOP                            \
                "st      %%o2, %0               \n\t"   \
                "stx     %%o1, %1               \n\t"   \
                "stx     %%o0, %2               \n\t"   \
        : "=m" (c), "=m" (d), "=m" (s)          \
        : "m" (s), "m" (d), "m" (c), "m" (b)    \
        : "g1", "o0", "o1", "o2", "o3", "o4",   \
          "o5"                                  \
        );

#else /* __sparc64__ */

#define MULADDC_INIT                                    \
    asm(                                                \
                "ld      %3, %%o0               \n\t"   \
                "ld      %4, %%o1               \n\t"   \
                "ld      %5, %%o2               \n\t"   \
                "ld      %6, %%o3               \n\t"

#define MULADDC_CORE                                    \
                "ld      [%%o0], %%o4           \n\t"   \
                "inc     4, %%o0                \n\t"   \
                "ld      [%%o1], %%o5           \n\t"   \
                "umul    %%o3, %%o4, %%o4       \n\t"   \
                "addcc   %%o4, %%o2, %%o4       \n\t"   \
                "rd      %%y, %%g1              \n\t"   \
                "addx    %%g1, 0, %%g1          \n\t"   \
                "addcc   %%o4, %%o5, %%o4       \n\t"   \
                "st      %%o4, [%%o1]           \n\t"   \
                "addx    %%g1, 0, %%o2          \n\t"   \
                "inc     4, %%o1                \n\t"

#define MULADDC_STOP                                    \
                "st      %%o2, %0               \n\t"   \
                "st      %%o1, %1               \n\t"   \
                "st      %%o0, %2               \n\t"   \
        : "=m" (c), "=m" (d), "=m" (s)          \
        : "m" (s), "m" (d), "m" (c), "m" (b)    \
        : "g1", "o0", "o1", "o2", "o3", "o4",   \
          "o5"                                  \
        );

#endif /* __sparc64__ */
#endif /* __sparc__ */

#if defined(__microblaze__) || defined(microblaze)

#define MULADDC_INIT                    \
    asm(                                \
        "lwi   r3,   %3         \n\t"   \
        "lwi   r4,   %4         \n\t"   \
        "lwi   r5,   %5         \n\t"   \
        "lwi   r6,   %6         \n\t"   \
        "andi  r7,   r6, 0xffff \n\t"   \
        "bsrli r6,   r6, 16     \n\t"

#define MULADDC_CORE                    \
        "lhui  r8,   r3,   0    \n\t"   \
        "addi  r3,   r3,   2    \n\t"   \
        "lhui  r9,   r3,   0    \n\t"   \
        "addi  r3,   r3,   2    \n\t"   \
        "mul   r10,  r9,  r6    \n\t"   \
        "mul   r11,  r8,  r7    \n\t"   \
        "mul   r12,  r9,  r7    \n\t"   \
        "mul   r13,  r8,  r6    \n\t"   \
        "bsrli  r8, r10,  16    \n\t"   \
        "bsrli  r9, r11,  16    \n\t"   \
        "add   r13, r13,  r8    \n\t"   \
        "add   r13, r13,  r9    \n\t"   \
        "bslli r10, r10,  16    \n\t"   \
        "bslli r11, r11,  16    \n\t"   \
        "add   r12, r12, r10    \n\t"   \
        "addc  r13, r13,  r0    \n\t"   \
        "add   r12, r12, r11    \n\t"   \
        "addc  r13, r13,  r0    \n\t"   \
        "lwi   r10,  r4,   0    \n\t"   \
        "add   r12, r12, r10    \n\t"   \
        "addc  r13, r13,  r0    \n\t"   \
        "add   r12, r12,  r5    \n\t"   \
        "addc   r5, r13,  r0    \n\t"   \
        "swi   r12,  r4,   0    \n\t"   \
        "addi   r4,  r4,   4    \n\t"

#define MULADDC_STOP                    \
        "swi   r5,   %0         \n\t"   \
        "swi   r4,   %1         \n\t"   \
        "swi   r3,   %2         \n\t"   \
        : "=m" (c), "=m" (d), "=m" (s)              \
        : "m" (s), "m" (d), "m" (c), "m" (b)        \
        : "r3", "r4"  "r5", "r6", "r7", "r8",       \
          "r9", "r10", "r11", "r12", "r13"          \
    );

#endif /* MicroBlaze */

#if defined(__tricore__)

#define MULADDC_INIT                            \
    asm(                                        \
        "ld.a   %%a2, %3                \n\t"   \
        "ld.a   %%a3, %4                \n\t"   \
        "ld.w   %%d4, %5                \n\t"   \
        "ld.w   %%d1, %6                \n\t"   \
        "xor    %%d5, %%d5              \n\t"

#define MULADDC_CORE                            \
        "ld.w   %%d0,   [%%a2+]         \n\t"   \
        "madd.u %%e2, %%e4, %%d0, %%d1  \n\t"   \
        "ld.w   %%d0,   [%%a3]          \n\t"   \
        "addx   %%d2,    %%d2,  %%d0    \n\t"   \
        "addc   %%d3,    %%d3,    0     \n\t"   \
        "mov    %%d4,    %%d3           \n\t"   \
        "st.w  [%%a3+],  %%d2           \n\t"

#define MULADDC_STOP                            \
        "st.w   %0, %%d4                \n\t"   \
        "st.a   %1, %%a3                \n\t"   \
        "st.a   %2, %%a2                \n\t"   \
        : "=m" (c), "=m" (d), "=m" (s)          \
        : "m" (s), "m" (d), "m" (c), "m" (b)    \
        : "d0", "d1", "e2", "d4", "a2", "a3"    \
    );

#endif /* TriCore */

/*
 * gcc -O0 by default uses r7 for the frame pointer, so it complains about our
 * use of r7 below, unless -fomit-frame-pointer is passed. Unfortunately,
 * passing that option is not easy when building with yotta.
 *
 * On the other hand, -fomit-frame-pointer is implied by any -Ox options with
 * x !=0, which we can detect using __OPTIMIZE__ (which is also defined by
 * clang and armcc5 under the same conditions).
 *
 * So, only use the optimized assembly below for optimized build, which avoids
 * the build error and is pretty reasonable anyway.
 */
#if defined(__GNUC__) && !defined(__OPTIMIZE__)
#define MULADDC_CANNOT_USE_R7
#endif

#if defined(__arm__) && !defined(MULADDC_CANNOT_USE_R7)

#if defined(__thumb__) && !defined(__thumb2__)

#define MULADDC_INIT                                    \
    asm(                                                \
            "ldr    r0, %3                      \n\t"   \
            "ldr    r1, %4                      \n\t"   \
            "ldr    r2, %5                      \n\t"   \
            "ldr    r3, %6                      \n\t"   \
            "lsr    r7, r3, #16                 \n\t"   \
            "mov    r9, r7                      \n\t"   \
            "lsl    r7, r3, #16                 \n\t"   \
            "lsr    r7, r7, #16                 \n\t"   \
            "mov    r8, r7                      \n\t"

#define MULADDC_CORE                                    \
            "ldmia  r0!, {r6}                   \n\t"   \
            "lsr    r7, r6, #16                 \n\t"   \
            "lsl    r6, r6, #16                 \n\t"   \
            "lsr    r6, r6, #16                 \n\t"   \
            "mov    r4, r8                      \n\t"   \
            "mul    r4, r6                      \n\t"   \
            "mov    r3, r9                      \n\t"   \
            "mul    r6, r3                      \n\t"   \
            "mov    r5, r9                      \n\t"   \
            "mul    r5, r7                      \n\t"   \
            "mov    r3, r8                      \n\t"   \
            "mul    r7, r3                      \n\t"   \
            "lsr    r3, r6, #16                 \n\t"   \
            "add    r5, r5, r3                  \n\t"   \
            "lsr    r3, r7, #16                 \n\t"   \
            "add    r5, r5, r3                  \n\t"   \
            "add    r4, r4, r2                  \n\t"   \
            "mov    r2, #0                      \n\t"   \
            "adc    r5, r2                      \n\t"   \
            "lsl    r3, r6, #16                 \n\t"   \
            "add    r4, r4, r3                  \n\t"   \
            "adc    r5, r2                      \n\t"   \
            "lsl    r3, r7, #16                 \n\t"   \
            "add    r4, r4, r3                  \n\t"   \
            "adc    r5, r2                      \n\t"   \
            "ldr    r3, [r1]                    \n\t"   \
            "add    r4, r4, r3                  \n\t"   \
            "adc    r2, r5                      \n\t"   \
            "stmia  r1!, {r4}                   \n\t"

#define MULADDC_STOP                                    \
            "str    r2, %0                      \n\t"   \
            "str    r1, %1                      \n\t"   \
            "str    r0, %2                      \n\t"   \
         : "=m" (c),  "=m" (d), "=m" (s)        \
         : "m" (s), "m" (d), "m" (c), "m" (b)   \
         : "r0", "r1", "r2", "r3", "r4", "r5",  \
           "r6", "r7", "r8", "r9", "cc"         \
         );

#else

#define MULADDC_INIT                                    \
    asm(                                                \
            "ldr    r0, %3                      \n\t"   \
            "ldr    r1, %4                      \n\t"   \
            "ldr    r2, %5                      \n\t"   \
            "ldr    r3, %6                      \n\t"

#define MULADDC_CORE                                    \
            "ldr    r4, [r0], #4                \n\t"   \
            "mov    r5, #0                      \n\t"   \
            "ldr    r6, [r1]                    \n\t"   \
            "umlal  r2, r5, r3, r4              \n\t"   \
            "adds   r7, r6, r2                  \n\t"   \
            "adc    r2, r5, #0                  \n\t"   \
            "str    r7, [r1], #4                \n\t"

#define MULADDC_STOP                                    \
            "str    r2, %0                      \n\t"   \
            "str    r1, %1                      \n\t"   \
            "str    r0, %2                      \n\t"   \
         : "=m" (c),  "=m" (d), "=m" (s)        \
         : "m" (s), "m" (d), "m" (c), "m" (b)   \
         : "r0", "r1", "r2", "r3", "r4", "r5",  \
           "r6", "r7", "cc"                     \
         );

#endif /* Thumb */

#endif /* ARMv3 */

#if defined(__alpha__)

#define MULADDC_INIT                    \
    asm(                                \
        "ldq    $1, %3          \n\t"   \
        "ldq    $2, %4          \n\t"   \
        "ldq    $3, %5          \n\t"   \
        "ldq    $4, %6          \n\t"

#define MULADDC_CORE                    \
        "ldq    $6,  0($1)      \n\t"   \
        "addq   $1,  8, $1      \n\t"   \
        "mulq   $6, $4, $7      \n\t"   \
        "umulh  $6, $4, $6      \n\t"   \
        "addq   $7, $3, $7      \n\t"   \
        "cmpult $7, $3, $3      \n\t"   \
        "ldq    $5,  0($2)      \n\t"   \
        "addq   $7, $5, $7      \n\t"   \
        "cmpult $7, $5, $5      \n\t"   \
        "stq    $7,  0($2)      \n\t"   \
        "addq   $2,  8, $2      \n\t"   \
        "addq   $6, $3, $3      \n\t"   \
        "addq   $5, $3, $3      \n\t"

#define MULADDC_STOP                                    \
        "stq    $3, %0          \n\t"   \
        "stq    $2, %1          \n\t"   \
        "stq    $1, %2          \n\t"   \
        : "=m" (c), "=m" (d), "=m" (s)              \
        : "m" (s), "m" (d), "m" (c), "m" (b)        \
        : "$1", "$2", "$3", "$4", "$5", "$6", "$7"  \
    );
#endif /* Alpha */

#if defined(__mips__) && !defined(__mips64)

#define MULADDC_INIT                    \
    asm(                                \
        "lw     $10, %3         \n\t"   \
        "lw     $11, %4         \n\t"   \
        "lw     $12, %5         \n\t"   \
        "lw     $13, %6         \n\t"

#define MULADDC_CORE                    \
        "lw     $14, 0($10)     \n\t"   \
        "multu  $13, $14        \n\t"   \
        "addi   $10, $10, 4     \n\t"   \
        "mflo   $14             \n\t"   \
        "mfhi   $9              \n\t"   \
        "addu   $14, $12, $14   \n\t"   \
        "lw     $15, 0($11)     \n\t"   \
        "sltu   $12, $14, $12   \n\t"   \
        "addu   $15, $14, $15   \n\t"   \
        "sltu   $14, $15, $14   \n\t"   \
        "addu   $12, $12, $9    \n\t"   \
        "sw     $15, 0($11)     \n\t"   \
        "addu   $12, $12, $14   \n\t"   \
        "addi   $11, $11, 4     \n\t"

#define MULADDC_STOP                    \
        "sw     $12, %0         \n\t"   \
        "sw     $11, %1         \n\t"   \
        "sw     $10, %2         \n\t"   \
        : "=m" (c), "=m" (d), "=m" (s)                      \
        : "m" (s), "m" (d), "m" (c), "m" (b)                \
        : "$9", "$10", "$11", "$12", "$13", "$14", "$15"    \
    );

#endif /* MIPS */
#endif /* GNUC */

#if (defined(_MSC_VER) && defined(_M_IX86)) || defined(__WATCOMC__)

#define MULADDC_INIT                            \
    __asm   mov     esi, s                      \
    __asm   mov     edi, d                      \
    __asm   mov     ecx, c                      \
    __asm   mov     ebx, b

#define MULADDC_CORE                            \
    __asm   lodsd                               \
    __asm   mul     ebx                         \
    __asm   add     eax, ecx                    \
    __asm   adc     edx, 0                      \
    __asm   add     eax, [edi]                  \
    __asm   adc     edx, 0                      \
    __asm   mov     ecx, edx                    \
    __asm   stosd

#if defined(BIGINT_HAVE_SSE2)

#define EMIT __asm _emit

#define MULADDC_HUIT                            \
    EMIT 0x0F  EMIT 0x6E  EMIT 0xC9             \
    EMIT 0x0F  EMIT 0x6E  EMIT 0xC3             \
    EMIT 0x0F  EMIT 0x6E  EMIT 0x1F             \
    EMIT 0x0F  EMIT 0xD4  EMIT 0xCB             \
    EMIT 0x0F  EMIT 0x6E  EMIT 0x16             \
    EMIT 0x0F  EMIT 0xF4  EMIT 0xD0             \
    EMIT 0x0F  EMIT 0x6E  EMIT 0x66  EMIT 0x04  \
    EMIT 0x0F  EMIT 0xF4  EMIT 0xE0             \
    EMIT 0x0F  EMIT 0x6E  EMIT 0x76  EMIT 0x08  \
    EMIT 0x0F  EMIT 0xF4  EMIT 0xF0             \
    EMIT 0x0F  EMIT 0x6E  EMIT 0x7E  EMIT 0x0C  \
    EMIT 0x0F  EMIT 0xF4  EMIT 0xF8             \
    EMIT 0x0F  EMIT 0xD4  EMIT 0xCA             \
    EMIT 0x0F  EMIT 0x6E  EMIT 0x5F  EMIT 0x04  \
    EMIT 0x0F  EMIT 0xD4  EMIT 0xDC             \
    EMIT 0x0F  EMIT 0x6E  EMIT 0x6F  EMIT 0x08  \
    EMIT 0x0F  EMIT 0xD4  EMIT 0xEE             \
    EMIT 0x0F  EMIT 0x6E  EMIT 0x67  EMIT 0x0C  \
    EMIT 0x0F  EMIT 0xD4  EMIT 0xFC             \
    EMIT 0x0F  EMIT 0x7E  EMIT 0x0F             \
    EMIT 0x0F  EMIT 0x6E  EMIT 0x56  EMIT 0x10  \
    EMIT 0x0F  EMIT 0xF4  EMIT 0xD0             \
    EMIT 0x0F  EMIT 0x73  EMIT 0xD1  EMIT 0x20  \
    EMIT 0x0F  EMIT 0x6E  EMIT 0x66  EMIT 0x14  \
    EMIT 0x0F  EMIT 0xF4  EMIT 0xE0             \
    EMIT 0x0F  EMIT 0xD4  EMIT 0xCB             \
    EMIT 0x0F  EMIT 0x6E  EMIT 0x76  EMIT 0x18  \
    EMIT 0x0F  EMIT 0xF4  EMIT 0xF0             \
    EMIT 0x0F  EMIT 0x7E  EMIT 0x4F  EMIT 0x04  \
    EMIT 0x0F  EMIT 0x73  EMIT 0xD1  EMIT 0x20  \
    EMIT 0x0F  EMIT 0x6E  EMIT 0x5E  EMIT 0x1C  \
    EMIT 0x0F  EMIT 0xF4  EMIT 0xD8             \
    EMIT 0x0F  EMIT 0xD4  EMIT 0xCD             \
    EMIT 0x0F  EMIT 0x6E  EMIT 0x6F  EMIT 0x10  \
    EMIT 0x0F  EMIT 0xD4  EMIT 0xD5             \
    EMIT 0x0F  EMIT 0x7E  EMIT 0x4F  EMIT 0x08  \
    EMIT 0x0F  EMIT 0x73  EMIT 0xD1  EMIT 0x20  \
    EMIT 0x0F  EMIT 0xD4  EMIT 0xCF             \
    EMIT 0x0F  EMIT 0x6E  EMIT 0x6F  EMIT 0x14  \
    EMIT 0x0F  EMIT 0xD4  EMIT 0xE5             \
    EMIT 0x0F  EMIT 0x7E  EMIT 0x4F  EMIT 0x0C  \
    EMIT 0x0F  EMIT 0x73  EMIT 0xD1  EMIT 0x20  \
    EMIT 0x0F  EMIT 0xD4  EMIT 0xCA             \
    EMIT 0x0F  EMIT 0x6E  EMIT 0x6F  EMIT 0x18  \
    EMIT 0x0F  EMIT 0xD4  EMIT 0xF5             \
    EMIT 0x0F  EMIT 0x7E  EMIT 0x4F  EMIT 0x10  \
    EMIT 0x0F  EMIT 0x73  EMIT 0xD1  EMIT 0x20  \
    EMIT 0x0F  EMIT 0xD4  EMIT 0xCC             \
    EMIT 0x0F  EMIT 0x6E  EMIT 0x6F  EMIT 0x1C  \
    EMIT 0x0F  EMIT 0xD4  EMIT 0xDD             \
    EMIT 0x0F  EMIT 0x7E  EMIT 0x4F  EMIT 0x14  \
    EMIT 0x0F  EMIT 0x73  EMIT 0xD1  EMIT 0x20  \
    EMIT 0x0F  EMIT 0xD4  EMIT 0xCE             \
    EMIT 0x0F  EMIT 0x7E  EMIT 0x4F  EMIT 0x18  \
    EMIT 0x0F  EMIT 0x73  EMIT 0xD1  EMIT 0x20  \
    EMIT 0x0F  EMIT 0xD4  EMIT 0xCB             \
    EMIT 0x0F  EMIT 0x7E  EMIT 0x4F  EMIT 0x1C  \
    EMIT 0x83  EMIT 0xC7  EMIT 0x20             \
    EMIT 0x83  EMIT 0xC6  EMIT 0x20             \
    EMIT 0x0F  EMIT 0x73  EMIT 0xD1  EMIT 0x20  \
    EMIT 0x0F  EMIT 0x7E  EMIT 0xC9

#define MULADDC_STOP                            \
    EMIT 0x0F  EMIT 0x77                        \
    __asm   mov     c, ecx                      \
    __asm   mov     d, edi                      \
    __asm   mov     s, esi                      \

#else

#define MULADDC_STOP                            \
    __asm   mov     c, ecx                      \
    __asm   mov     d, edi                      \
    __asm   mov     s, esi                      \

#endif /* SSE2 */
#endif /* MSVC */

#endif /* BIGINT_HAVE_ASM */

#if !defined(MULADDC_CORE)
#if defined(BIGINT_HAVE_UDBL)

#define MULADDC_INIT                    \
{                                       \
    bigint_udbl_t r;                   \
    bigint_uint_t r0, r1;

#define MULADDC_CORE                    \
    r   = *(s++) * (bigint_udbl_t) b;  \
    r0  = (bigint_uint_t) r;            \
    r1  = (bigint_uint_t)( r >> biL );  \
    r0 += c;  r1 += (r0 <  c);          \
    r0 += *d; r1 += (r0 < *d);          \
    c = r1; *(d++) = r0;

#define MULADDC_STOP                    \
}

#else
#define MULADDC_INIT                    \
{                                       \
    bigint_uint_t s0, s1, b0, b1;       \
    bigint_uint_t r0, r1, rx, ry;       \
    b0 = ( b << biH ) >> biH;           \
    b1 = ( b >> biH );

#define MULADDC_CORE                    \
    s0 = ( *s << biH ) >> biH;          \
    s1 = ( *s >> biH ); s++;            \
    rx = s0 * b1; r0 = s0 * b0;         \
    ry = s1 * b0; r1 = s1 * b1;         \
    r1 += ( rx >> biH );                \
    r1 += ( ry >> biH );                \
    rx <<= biH; ry <<= biH;             \
    r0 += rx; r1 += (r0 < rx);          \
    r0 += ry; r1 += (r0 < ry);          \
    r0 +=  c; r1 += (r0 <  c);          \
    r0 += *d; r1 += (r0 < *d);          \
    c = r1; *(d++) = r0;

#define MULADDC_STOP                    \
}

#endif /* C (generic)  */
#endif /* C (longlong) */


/*****************************************************************************
 * The content of mbedtls "bignum.c"
 *****************************************************************************/
/*
 *  Multi-precision integer library
 *
 *  Copyright (C) 2006-2015, ARM Limited, All Rights Reserved
 *  SPDX-License-Identifier: Apache-2.0
 *
 *  Licensed under the Apache License, Version 2.0 (the "License"); you may
 *  not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 *  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 *  This file is part of mbed TLS (https://tls.mbed.org)
 */

/*
 *  The following sources were referenced in the design of this Multi-precision
 *  Integer library:
 *
 *  [1] Handbook of Applied Cryptography - 1997
 *      Menezes, van Oorschot and Vanstone
 *
 *  [2] Multi-Precision Math
 *      Tom St Denis
 *      https://github.com/libtom/libtommath/blob/develop/tommath.pdf
 *
 *  [3] GNU Multi-Precision Arithmetic Library
 *      https://gmplib.org/manual/index.html
 *
 */

#define mbedtls_calloc      calloc
#define mbedtls_free        free

/* Implementation that should never be optimized out by the compiler */
static void mbedtls_mpi_zeroize( bigint_uint_t *v, size_t n ) {
    volatile bigint_uint_t *p = v; while( n-- ) *p++ = 0;
}

#define ciL    (sizeof(bigint_uint_t))         /* chars in limb  */
#define biL    (ciL << 3)               /* bits  in limb  */
#define biH    (ciL << 2)               /* half limb size */

#define MPI_SIZE_T_MAX  ( (size_t) -1 ) /* SIZE_T_MAX is not standard */

/*
 * Convert between bits/chars and number of limbs
 * Divide first in order to avoid potential overflows
 */
#define BITS_TO_LIMBS(i)  ( (i) / biL + ( (i) % biL != 0 ) )
#define CHARS_TO_LIMBS(i) ( (i) / ciL + ( (i) % ciL != 0 ) )

/*
 * Initialize one MPI
 */
void Bigint::mbedtls::mpi_init( Bigint::Data *X )
{
    if( X == NULL )
        return;

    X->s = 1;
    X->n = 0;
    X->p = NULL;
}

/*
 * Unallocate one MPI
 */
void Bigint::mbedtls::mpi_free( Bigint::Data *X )
{
    if( X == NULL )
        return;

    if( X->p != NULL )
    {
        mbedtls_mpi_zeroize( X->p, X->n );
        mbedtls_free( X->p );
    }

    X->s = 1;
    X->n = 0;
    X->p = NULL;
}

/*
 * Enlarge to the specified number of limbs
 */
int Bigint::mbedtls::mpi_grow( Bigint::Data *X, size_t nblimbs )
{
    bigint_uint_t *p;

    if( nblimbs > BIGINT_MPI_MAX_LIMBS )
        return( BIGINT_ERR_MPI_ALLOC_FAILED );

    if( X->n < nblimbs )
    {
        if( ( p = (bigint_uint_t*)mbedtls_calloc( nblimbs, ciL ) ) == NULL )
            return( BIGINT_ERR_MPI_ALLOC_FAILED );

        if( X->p != NULL )
        {
            memcpy( p, X->p, X->n * ciL );
            mbedtls_mpi_zeroize( X->p, X->n );
            mbedtls_free( X->p );
        }

        X->n = nblimbs;
        X->p = p;
    }

    return( 0 );
}

/*
 * Resize down as much as possible,
 * while keeping at least the specified number of limbs
 */
int Bigint::mbedtls::mpi_shrink( Bigint::Data *X, size_t nblimbs )
{
    bigint_uint_t *p;
    size_t i;

    /* Actually resize up in this case */
    if( X->n <= nblimbs )
        return( mpi_grow( X, nblimbs ) );

    for( i = X->n - 1; i > 0; i-- )
        if( X->p[i] != 0 )
            break;
    i++;

    if( i < nblimbs )
        i = nblimbs;

    if( ( p = (bigint_uint_t*)mbedtls_calloc( i, ciL ) ) == NULL )
        return( BIGINT_ERR_MPI_ALLOC_FAILED );

    if( X->p != NULL )
    {
        memcpy( p, X->p, i * ciL );
        mbedtls_mpi_zeroize( X->p, X->n );
        mbedtls_free( X->p );
    }

    X->n = i;
    X->p = p;

    return( 0 );
}

/*
 * Copy the contents of Y into X
 */
int Bigint::mbedtls::mpi_copy( Bigint::Data *X, const Bigint::Data *Y )
{
    int ret;
    size_t i;

    if( X == Y )
        return( 0 );

    if( Y->p == NULL )
    {
        mpi_free( X );
        return( 0 );
    }

    for( i = Y->n - 1; i > 0; i-- )
        if( Y->p[i] != 0 )
            break;
    i++;

    X->s = Y->s;

    BIGINT_MPI_CHK( mpi_grow( X, i ) );

    memset( X->p, 0, X->n * ciL );
    memcpy( X->p, Y->p, i * ciL );

cleanup:

    return( ret );
}

/*
 * Swap the contents of X and Y
 */
void Bigint::mbedtls::mpi_swap( Bigint::Data *X, Bigint::Data *Y )
{
    Bigint::Data T;

    memcpy( &T,  X, sizeof( Bigint::Data ) );
    memcpy(  X,  Y, sizeof( Bigint::Data ) );
    memcpy(  Y, &T, sizeof( Bigint::Data ) );
}

#if defined(FULL_BIGINT_LIB)
/*
 * Conditionally assign X = Y, without leaking information
 * about whether the assignment was made or not.
 * (Leaking information about the respective sizes of X and Y is ok however.)
 */
int Bigint::mbedtls::mpi_safe_cond_assign( Bigint::Data *X, const Bigint::Data *Y, unsigned char assign )
{
    int ret = 0;
    size_t i;

    /* make sure assign is 0 or 1 in a time-constant manner */
    assign = (assign | (unsigned char)-assign) >> 7;

    BIGINT_MPI_CHK( mpi_grow( X, Y->n ) );

    X->s = X->s * ( 1 - assign ) + Y->s * assign;

    for( i = 0; i < Y->n; i++ )
        X->p[i] = X->p[i] * ( 1 - assign ) + Y->p[i] * assign;

    for( ; i < X->n; i++ )
        X->p[i] *= ( 1 - assign );

cleanup:
    return( ret );
}

/*
 * Conditionally swap X and Y, without leaking information
 * about whether the swap was made or not.
 * Here it is not ok to simply swap the pointers, which whould lead to
 * different memory access patterns when X and Y are used afterwards.
 */
int Bigint::mbedtls::mpi_safe_cond_swap( Bigint::Data *X, Bigint::Data *Y, unsigned char swap )
{
    int ret, s;
    size_t i;
    bigint_uint_t tmp;

    if( X == Y )
        return( 0 );

    /* make sure swap is 0 or 1 in a time-constant manner */
    swap = (swap | (unsigned char)-swap) >> 7;

    BIGINT_MPI_CHK( mpi_grow( X, Y->n ) );
    BIGINT_MPI_CHK( mpi_grow( Y, X->n ) );

    s = X->s;
    X->s = X->s * ( 1 - swap ) + Y->s * swap;
    Y->s = Y->s * ( 1 - swap ) +    s * swap;


    for( i = 0; i < X->n; i++ )
    {
        tmp = X->p[i];
        X->p[i] = X->p[i] * ( 1 - swap ) + Y->p[i] * swap;
        Y->p[i] = Y->p[i] * ( 1 - swap ) +     tmp * swap;
    }

cleanup:
    return( ret );
}
#endif
/*
 * Set value from integer
 */
int Bigint::mbedtls::mpi_lset( Bigint::Data *X, bigint_sint_t z )
{
    int ret;

    BIGINT_MPI_CHK( mpi_grow( X, 1 ) );
    memset( X->p, 0, X->n * ciL );

    X->p[0] = ( z < 0 ) ? -z : z;
    X->s    = ( z < 0 ) ? -1 : 1;

cleanup:

    return( ret );
}

/*
 * Get a specific bit
 */
int Bigint::mbedtls::mpi_get_bit( const Bigint::Data *X, size_t pos )
{
    if( X->n * biL <= pos )
        return( 0 );

    return( ( X->p[pos / biL] >> ( pos % biL ) ) & 0x01 );
}

/*
 * Set a bit to a specific value of 0 or 1
 */
int Bigint::mbedtls::mpi_set_bit( Bigint::Data *X, size_t pos, unsigned char val )
{
    int ret = 0;
    size_t off = pos / biL;
    size_t idx = pos % biL;

    if( val != 0 && val != 1 )
        return( BIGINT_ERR_MPI_BAD_INPUT_DATA );

    if( X->n * biL <= pos )
    {
        if( val == 0 )
            return( 0 );

        BIGINT_MPI_CHK( mpi_grow( X, off + 1 ) );
    }

    X->p[off] &= ~( (bigint_uint_t) 0x01 << idx );
    X->p[off] |= (bigint_uint_t) val << idx;

cleanup:

    return( ret );
}

/*
 * Return the number of less significant zero-bits
 */
size_t Bigint::mbedtls::mpi_lsb( const Bigint::Data *X )
{
    size_t i, j, count = 0;

    for( i = 0; i < X->n; i++ )
        for( j = 0; j < biL; j++, count++ )
            if( ( ( X->p[i] >> j ) & 1 ) != 0 )
                return( count );

    return( 0 );
}

/*
 * Count leading zero bits in a given integer
 */
static size_t mbedtls_clz( const bigint_uint_t x )
{
    size_t j;
    bigint_uint_t mask = (bigint_uint_t) 1 << (biL - 1);

    for( j = 0; j < biL; j++ )
    {
        if( x & mask ) break;

        mask >>= 1;
    }

    return j;
}

/*
 * Return the number of bits
 */
size_t Bigint::mbedtls::mpi_bitlen( const Bigint::Data *X )
{
    size_t i, j;

    if( X->n == 0 )
        return( 0 );

    for( i = X->n - 1; i > 0; i-- )
        if( X->p[i] != 0 )
            break;

    j = biL - mbedtls_clz( X->p[i] );

    return( ( i * biL ) + j );
}

/*
 * Return the total size in bytes
 */
size_t Bigint::mbedtls::mpi_size( const Bigint::Data *X )
{
    return( ( mpi_bitlen( X ) + 7 ) >> 3 );
}

/*
 * Convert an ASCII character to digit value
 */
static int mpi_get_digit( bigint_uint_t *d, int radix, char c )
{
    *d = 255;

    if( c >= 0x30 && c <= 0x39 ) *d = c - 0x30;
    if( c >= 0x41 && c <= 0x46 ) *d = c - 0x37;
    if( c >= 0x61 && c <= 0x66 ) *d = c - 0x57;

    if( *d >= (bigint_uint_t) radix )
        return( BIGINT_ERR_MPI_INVALID_CHARACTER );

    return( 0 );
}

static int mpi_get_digit( bigint_uint_t *d, int radix, QChar c )
{
    *d = 255;

    if( c.unicode() >= 0x30 && c.unicode() <= 0x39 ) *d = c.unicode() - 0x30;
    if( c.unicode() >= 0x41 && c.unicode() <= 0x46 ) *d = c.unicode() - 0x37;
    if( c.unicode() >= 0x61 && c.unicode() <= 0x66 ) *d = c.unicode() - 0x57;

    if( *d >= (bigint_uint_t) radix )
        return( BIGINT_ERR_MPI_INVALID_CHARACTER );

    return( 0 );
}

/*
 * Import from an ASCII string
 */
template <typename Tchar>
int Bigint::mbedtls::mpi_read_string( Bigint::Data *X, int radix, const Tchar *s, size_t slen)
{
    int ret;
    size_t i, j, n;
    bigint_uint_t d;
    Bigint::Data T;

    if( radix < 2 || radix > 16 )
        return( BIGINT_ERR_MPI_BAD_INPUT_DATA );

    mpi_init( &T );

    if( radix == 16 )
    {
        if( slen > MPI_SIZE_T_MAX >> 2 )
            return( BIGINT_ERR_MPI_BAD_INPUT_DATA );

        n = BITS_TO_LIMBS( slen << 2 );

        BIGINT_MPI_CHK( mpi_grow( X, n ) );
        BIGINT_MPI_CHK( mpi_lset( X, 0 ) );

        for( i = slen, j = 0; i > 0; i--, j++ )
        {
            if( i == 1 && s[i - 1] == '-' )
            {
                X->s = -1;
                break;
            }

            BIGINT_MPI_CHK( mpi_get_digit( &d, radix, s[i - 1] ) );
            X->p[j / ( 2 * ciL )] |= d << ( ( j % ( 2 * ciL ) ) << 2 );
        }
    }
    else
    {
        BIGINT_MPI_CHK( mpi_lset( X, 0 ) );

        for( i = 0; i < slen; i++ )
        {
            if( i == 0 && s[i] == '-' )
            {
                X->s = -1;
                continue;
            }

            BIGINT_MPI_CHK( mpi_get_digit( &d, radix, s[i] ) );
            BIGINT_MPI_CHK( mpi_mul_int( &T, X, radix ) );

            if( X->s == 1 )
            {
                BIGINT_MPI_CHK( mpi_add_int( X, &T, d ) );
            }
            else
            {
                BIGINT_MPI_CHK( mpi_sub_int( X, &T, d ) );
            }
        }
    }

cleanup:

    mpi_free( &T );

    return( ret );
}

/*
 * Helper to write the digits high-order first
 */
template <typename Tchar>
int Bigint::mbedtls::mpi_write_hlp( Bigint::Data *X, int radix, Tchar **p )
{
    int ret;
    bigint_uint_t r;

    if( radix < 2 || radix > 16 )
        return( BIGINT_ERR_MPI_BAD_INPUT_DATA );

    BIGINT_MPI_CHK( mpi_mod_int( &r, X, radix ) );
    BIGINT_MPI_CHK( mpi_div_int( X, NULL, X, radix ) );

    if( mpi_cmp_int( X, 0 ) != 0 )
        BIGINT_MPI_CHK( mpi_write_hlp( X, radix, p ) );

    if( r < 10 )
        *(*p)++ = Tchar((char)( r + 0x30 ));
    else
        *(*p)++ = Tchar((char)( r + 0x37 ));

cleanup:

    return( ret );
}

/*
 * Export into an ASCII string
 */
template <typename Tchar>
int Bigint::mbedtls::mpi_write_string( const Bigint::Data *X, int radix,
                              Tchar *buf, size_t buflen, size_t *olen )
{
    int ret = 0;
    size_t n;
    Tchar *p;
    Bigint::Data T;

    if( radix < 2 || radix > 16 )
        return( BIGINT_ERR_MPI_BAD_INPUT_DATA );

    n = mpi_bitlen( X );
    if( radix >=  4 ) n >>= 1;
    if( radix >= 16 ) n >>= 1;
    n += 3;

    if( buflen < n )
    {
        *olen = n;
        return( BIGINT_ERR_MPI_BUFFER_TOO_SMALL );
    }

    p = buf;
    mpi_init( &T );

    if( X->s == -1 )
        *p++ = '-';

    if( radix == 16 )
    {
        int c;
        size_t i, j, k;

        for( i = X->n, k = 0; i > 0; i-- )
        {
            for( j = ciL; j > 0; j-- )
            {
                c = ( X->p[i - 1] >> ( ( j - 1 ) << 3) ) & 0xFF;

                if( c == 0 && k == 0 && ( i + j ) != 2 )
                    continue;

                *(p++) = Tchar("0123456789ABCDEF" [c / 16]);
                *(p++) = Tchar("0123456789ABCDEF" [c % 16]);
                k = 1;
            }
        }
    }
    else
    {
        BIGINT_MPI_CHK( mpi_copy( &T, X ) );

        if( T.s == -1 )
            T.s = 1;

        BIGINT_MPI_CHK( mpi_write_hlp( &T, radix, &p ) );
    }

    *p++ = '\0';
    *olen = p - buf;

cleanup:

    mpi_free( &T );

    return( ret );
}

/*
 * Import X from unsigned binary data, big endian
 */
int Bigint::mbedtls::mpi_read_binary( Bigint::Data *X, const unsigned char *buf, size_t buflen )
{
    int ret;
    size_t i, j, n;

    for( n = 0; n < buflen; n++ )
        if( buf[n] != 0 )
            break;

    BIGINT_MPI_CHK( mpi_grow( X, CHARS_TO_LIMBS( buflen - n ) ) );
    BIGINT_MPI_CHK( mpi_lset( X, 0 ) );

    for( i = buflen, j = 0; i > n; i--, j++ )
        X->p[j / ciL] |= ((bigint_uint_t) buf[i - 1]) << ((j % ciL) << 3);

cleanup:

    return( ret );
}

/*
 * Export X into unsigned binary data, big endian
 */
int Bigint::mbedtls::mpi_write_binary( const Bigint::Data *X, unsigned char *buf, size_t buflen )
{
    size_t i, j, n;

    n = mpi_size( X );

    if( buflen < n )
        return( BIGINT_ERR_MPI_BUFFER_TOO_SMALL );

    memset( buf, 0, buflen );

    for( i = buflen - 1, j = 0; n > 0; i--, j++, n-- )
        buf[i] = (unsigned char)( X->p[j / ciL] >> ((j % ciL) << 3) );

    return( 0 );
}

/*
 * Left-shift: X <<= count
 */
int Bigint::mbedtls::mpi_shift_l( Bigint::Data *X, size_t count )
{
    int ret;
    size_t i, v0, t1;
    bigint_uint_t r0 = 0, r1;

    v0 = count / (biL    );
    t1 = count & (biL - 1);

    i = mpi_bitlen( X ) + count;

    if( X->n * biL < i )
        BIGINT_MPI_CHK( mpi_grow( X, BITS_TO_LIMBS( i ) ) );

    ret = 0;

    /*
     * shift by count / limb_size
     */
    if( v0 > 0 )
    {
        for( i = X->n; i > v0; i-- )
            X->p[i - 1] = X->p[i - v0 - 1];

        for( ; i > 0; i-- )
            X->p[i - 1] = 0;
    }

    /*
     * shift by count % limb_size
     */
    if( t1 > 0 )
    {
        for( i = v0; i < X->n; i++ )
        {
            r1 = X->p[i] >> (biL - t1);
            X->p[i] <<= t1;
            X->p[i] |= r0;
            r0 = r1;
        }
    }

cleanup:

    return( ret );
}

/*
 * Right-shift: X >>= count
 */
int Bigint::mbedtls::mpi_shift_r( Bigint::Data *X, size_t count )
{
    size_t i, v0, v1;
    bigint_uint_t r0 = 0, r1;

    v0 = count /  biL;
    v1 = count & (biL - 1);

    if( v0 > X->n || ( v0 == X->n && v1 > 0 ) )
        return mpi_lset( X, 0 );

    /*
     * shift by count / limb_size
     */
    if( v0 > 0 )
    {
        for( i = 0; i < X->n - v0; i++ )
            X->p[i] = X->p[i + v0];

        for( ; i < X->n; i++ )
            X->p[i] = 0;
    }

    /*
     * shift by count % limb_size
     */
    if( v1 > 0 )
    {
        for( i = X->n; i > 0; i-- )
        {
            r1 = X->p[i - 1] << (biL - v1);
            X->p[i - 1] >>= v1;
            X->p[i - 1] |= r0;
            r0 = r1;
        }
    }

    return( 0 );
}

/*
 * Compare unsigned values
 */
int Bigint::mbedtls::mpi_cmp_abs( const Bigint::Data *X, const Bigint::Data *Y )
{
    size_t i, j;

    for( i = X->n; i > 0; i-- )
        if( X->p[i - 1] != 0 )
            break;

    for( j = Y->n; j > 0; j-- )
        if( Y->p[j - 1] != 0 )
            break;

    if( i == 0 && j == 0 )
        return( 0 );

    if( i > j ) return(  1 );
    if( j > i ) return( -1 );

    for( ; i > 0; i-- )
    {
        if( X->p[i - 1] > Y->p[i - 1] ) return(  1 );
        if( X->p[i - 1] < Y->p[i - 1] ) return( -1 );
    }

    return( 0 );
}

/*
 * Compare signed values
 */
int Bigint::mbedtls::mpi_cmp_mpi( const Bigint::Data *X, const Bigint::Data *Y )
{
    size_t i, j;

    for( i = X->n; i > 0; i-- )
        if( X->p[i - 1] != 0 )
            break;

    for( j = Y->n; j > 0; j-- )
        if( Y->p[j - 1] != 0 )
            break;

    if( i == 0 && j == 0 )
        return( 0 );

    if( i > j ) return(  X->s );
    if( j > i ) return( -Y->s );

    if( X->s > 0 && Y->s < 0 ) return(  1 );
    if( Y->s > 0 && X->s < 0 ) return( -1 );

    for( ; i > 0; i-- )
    {
        if( X->p[i - 1] > Y->p[i - 1] ) return(  X->s );
        if( X->p[i - 1] < Y->p[i - 1] ) return( -X->s );
    }

    return( 0 );
}

/*
 * Compare signed values
 */
int Bigint::mbedtls::mpi_cmp_int( const Bigint::Data *X, bigint_sint_t z )
{
    Bigint::Data Y;
    bigint_uint_t p[1];

    *p  = ( z < 0 ) ? -z : z;
    Y.s = ( z < 0 ) ? -1 : 1;
    Y.n = 1;
    Y.p = p;

    return( mpi_cmp_mpi( X, &Y ) );
}

/*
 * Unsigned addition: X = |A| + |B|  (HAC 14.7)
 */
int Bigint::mbedtls::mpi_add_abs( Bigint::Data *X, const Bigint::Data *A, const Bigint::Data *B )
{
    int ret;
    size_t i, j;
    bigint_uint_t *o, *p, c, tmp;

    if( X == B )
    {
        const Bigint::Data *T = A; A = X; B = T;
    }

    if( X != A )
        BIGINT_MPI_CHK( mpi_copy( X, A ) );

    /*
     * X should always be positive as a result of unsigned additions.
     */
    X->s = 1;

    for( j = B->n; j > 0; j-- )
        if( B->p[j - 1] != 0 )
            break;

    BIGINT_MPI_CHK( mpi_grow( X, j ) );

    o = B->p; p = X->p; c = 0;

    /*
     * tmp is used because it might happen that p == o
     */
    for( i = 0; i < j; i++, o++, p++ )
    {
        tmp= *o;
        *p +=  c; c  = ( *p <  c );
        *p += tmp; c += ( *p < tmp );
    }

    while( c != 0 )
    {
        if( i >= X->n )
        {
            BIGINT_MPI_CHK( mpi_grow( X, i + 1 ) );
            p = X->p + i;
        }

        *p += c; c = ( *p < c ); i++; p++;
    }

cleanup:

    return( ret );
}


/*
 * Helper for mbedtls_mpi subtraction
 */
static void mpi_sub_hlp( size_t n, bigint_uint_t *s, bigint_uint_t *d )
{
    size_t i;
    bigint_uint_t c, z;

    for( i = c = 0; i < n; i++, s++, d++ )
    {
        z = ( *d <  c );     *d -=  c;
        c = ( *d < *s ) + z; *d -= *s;
    }

    while( c != 0 )
    {
        z = ( *d < c ); *d -= c;
        c = z; i++; d++;
    }
}

/*
 * Unsigned subtraction: X = |A| - |B|  (HAC 14.9)
 */
int Bigint::mbedtls::mpi_sub_abs( Bigint::Data *X, const Bigint::Data *A, const Bigint::Data *B )
{
    Bigint::Data TB;
    int ret;
    size_t n;

    if( mpi_cmp_abs( A, B ) < 0 )
        return( BIGINT_ERR_MPI_NEGATIVE_VALUE );

    mpi_init( &TB );

    if( X == B )
    {
        BIGINT_MPI_CHK( mpi_copy( &TB, B ) );
        B = &TB;
    }

    if( X != A )
        BIGINT_MPI_CHK( mpi_copy( X, A ) );

    /*
     * X should always be positive as a result of unsigned subtractions.
     */
    X->s = 1;

    ret = 0;

    for( n = B->n; n > 0; n-- )
        if( B->p[n - 1] != 0 )
            break;

    mpi_sub_hlp( n, B->p, X->p );

cleanup:

    mpi_free( &TB );

    return( ret );
}

/*
 * Signed addition: X = A + B
 */
int Bigint::mbedtls::mpi_add_mpi( Bigint::Data *X, const Bigint::Data *A, const Bigint::Data *B )
{
    int ret, s = A->s;

    if( A->s * B->s < 0 )
    {
        if( mpi_cmp_abs( A, B ) >= 0 )
        {
            BIGINT_MPI_CHK( mpi_sub_abs( X, A, B ) );
            X->s =  s;
        }
        else
        {
            BIGINT_MPI_CHK( mpi_sub_abs( X, B, A ) );
            X->s = -s;
        }
    }
    else
    {
        BIGINT_MPI_CHK( mpi_add_abs( X, A, B ) );
        X->s = s;
    }

cleanup:

    return( ret );
}

/*
 * Signed subtraction: X = A - B
 */
int Bigint::mbedtls::mpi_sub_mpi( Bigint::Data *X, const Bigint::Data *A, const Bigint::Data *B )
{
    int ret, s = A->s;

    if( A->s * B->s > 0 )
    {
        if( mpi_cmp_abs( A, B ) >= 0 )
        {
            BIGINT_MPI_CHK( mpi_sub_abs( X, A, B ) );
            X->s =  s;
        }
        else
        {
            BIGINT_MPI_CHK( mpi_sub_abs( X, B, A ) );
            X->s = -s;
        }
    }
    else
    {
        BIGINT_MPI_CHK( mpi_add_abs( X, A, B ) );
        X->s = s;
    }

cleanup:

    return( ret );
}

/*
 * Signed addition: X = A + b
 */
int Bigint::mbedtls::mpi_add_int( Bigint::Data *X, const Bigint::Data *A, bigint_sint_t b )
{
    Bigint::Data _B;
    bigint_uint_t p[1];

    p[0] = ( b < 0 ) ? -b : b;
    _B.s = ( b < 0 ) ? -1 : 1;
    _B.n = 1;
    _B.p = p;

    return( mpi_add_mpi( X, A, &_B ) );
}

/*
 * Signed subtraction: X = A - b
 */
int Bigint::mbedtls::mpi_sub_int( Bigint::Data *X, const Bigint::Data *A, bigint_sint_t b )
{
    Bigint::Data _B;
    bigint_uint_t p[1];

    p[0] = ( b < 0 ) ? -b : b;
    _B.s = ( b < 0 ) ? -1 : 1;
    _B.n = 1;
    _B.p = p;

    return( mpi_sub_mpi( X, A, &_B ) );
}

/*
 * Helper for mbedtls_mpi multiplication
 */
static
#if defined(__APPLE__) && defined(__arm__)
/*
 * Apple LLVM version 4.2 (clang-425.0.24) (based on LLVM 3.2svn)
 * appears to need this to prevent bad ARM code generation at -O3.
 */
__attribute__ ((noinline))
#endif
void mpi_mul_hlp( size_t i, bigint_uint_t *s, bigint_uint_t *d, bigint_uint_t b )
{
    bigint_uint_t c = 0, t = 0;

#if defined(MULADDC_HUIT)
    for( ; i >= 8; i -= 8 )
    {
        MULADDC_INIT
        MULADDC_HUIT
        MULADDC_STOP
    }

    for( ; i > 0; i-- )
    {
        MULADDC_INIT
        MULADDC_CORE
        MULADDC_STOP
    }
#else /* MULADDC_HUIT */
    for( ; i >= 16; i -= 16 )
    {
        MULADDC_INIT
        MULADDC_CORE   MULADDC_CORE
        MULADDC_CORE   MULADDC_CORE
        MULADDC_CORE   MULADDC_CORE
        MULADDC_CORE   MULADDC_CORE

        MULADDC_CORE   MULADDC_CORE
        MULADDC_CORE   MULADDC_CORE
        MULADDC_CORE   MULADDC_CORE
        MULADDC_CORE   MULADDC_CORE
        MULADDC_STOP
    }

    for( ; i >= 8; i -= 8 )
    {
        MULADDC_INIT
        MULADDC_CORE   MULADDC_CORE
        MULADDC_CORE   MULADDC_CORE

        MULADDC_CORE   MULADDC_CORE
        MULADDC_CORE   MULADDC_CORE
        MULADDC_STOP
    }

    for( ; i > 0; i-- )
    {
        MULADDC_INIT
        MULADDC_CORE
        MULADDC_STOP
    }
#endif /* MULADDC_HUIT */

    t++;

    do {
        *d += c; c = ( *d < c ); d++;
    }
    while( c != 0 );
}

/*
 * Baseline multiplication: X = A * B  (HAC 14.12)
 */
int Bigint::mbedtls::mpi_mul_mpi( Bigint::Data *X, const Bigint::Data *A, const Bigint::Data *B )
{
    int ret;
    size_t i, j;
    Bigint::Data TA, TB;

    mpi_init( &TA ); mpi_init( &TB );

    if( X == A ) { BIGINT_MPI_CHK( mpi_copy( &TA, A ) ); A = &TA; }
    if( X == B ) { BIGINT_MPI_CHK( mpi_copy( &TB, B ) ); B = &TB; }

    for( i = A->n; i > 0; i-- )
        if( A->p[i - 1] != 0 )
            break;

    for( j = B->n; j > 0; j-- )
        if( B->p[j - 1] != 0 )
            break;

    BIGINT_MPI_CHK( mpi_grow( X, i + j ) );
    BIGINT_MPI_CHK( mpi_lset( X, 0 ) );

    for( i++; j > 0; j-- )
        mpi_mul_hlp( i - 1, A->p, X->p + j - 1, B->p[j - 1] );

    X->s = A->s * B->s;

cleanup:

    mpi_free( &TB ); mpi_free( &TA );

    return( ret );
}

/*
 * Baseline multiplication: X = A * b
 */
int Bigint::mbedtls::mpi_mul_int( Bigint::Data *X, const Bigint::Data *A, bigint_uint_t b )
{
    Bigint::Data _B;
    bigint_uint_t p[1];

    _B.s = 1;
    _B.n = 1;
    _B.p = p;
    p[0] = b;

    return( mpi_mul_mpi( X, A, &_B ) );
}

/*
 * Unsigned integer divide - double bigint_uint_t dividend, u1/u0, and
 * bigint_uint_t divisor, d
 */
static bigint_uint_t mbedtls_int_div_int( bigint_uint_t u1,
            bigint_uint_t u0, bigint_uint_t d, bigint_uint_t *r )
{
#if defined(BIGINT_HAVE_UDBL)
    bigint_udbl_t dividend, quotient;
#else
    const bigint_uint_t radix = (bigint_uint_t) 1 << biH;
    const bigint_uint_t uint_halfword_mask = ( (bigint_uint_t) 1 << biH ) - 1;
    bigint_uint_t d0, d1, q0, q1, rAX, r0, quotient;
    bigint_uint_t u0_msw, u0_lsw;
    size_t s;
#endif

    /*
     * Check for overflow
     */
    if( 0 == d || u1 >= d )
    {
        if (r != NULL) *r = ~0;

        return ( ~0 );
    }

#if defined(BIGINT_HAVE_UDBL)
    dividend  = (bigint_udbl_t) u1 << biL;
    dividend |= (bigint_udbl_t) u0;
    quotient = dividend / d;
    if( quotient > ( (bigint_udbl_t) 1 << biL ) - 1 )
        quotient = ( (bigint_udbl_t) 1 << biL ) - 1;

    if( r != NULL )
        *r = (bigint_uint_t)( dividend - (quotient * d ) );

    return (bigint_uint_t) quotient;
#else

    /*
     * Algorithm D, Section 4.3.1 - The Art of Computer Programming
     *   Vol. 2 - Seminumerical Algorithms, Knuth
     */

    /*
     * Normalize the divisor, d, and dividend, u0, u1
     */
    s = mbedtls_clz( d );
    d = d << s;

    u1 = u1 << s;
    u1 |= ( u0 >> ( biL - s ) ) & ( -(bigint_sint_t)s >> ( biL - 1 ) );
    u0 =  u0 << s;

    d1 = d >> biH;
    d0 = d & uint_halfword_mask;

    u0_msw = u0 >> biH;
    u0_lsw = u0 & uint_halfword_mask;

    /*
     * Find the first quotient and remainder
     */
    q1 = u1 / d1;
    r0 = u1 - d1 * q1;

    while( q1 >= radix || ( q1 * d0 > radix * r0 + u0_msw ) )
    {
        q1 -= 1;
        r0 += d1;

        if ( r0 >= radix ) break;
    }

    rAX = ( u1 * radix ) + ( u0_msw - q1 * d );
    q0 = rAX / d1;
    r0 = rAX - q0 * d1;

    while( q0 >= radix || ( q0 * d0 > radix * r0 + u0_lsw ) )
    {
        q0 -= 1;
        r0 += d1;

        if ( r0 >= radix ) break;
    }

    if (r != NULL)
        *r = ( rAX * radix + u0_lsw - q0 * d ) >> s;

    quotient = q1 * radix + q0;

    return quotient;
#endif
}

/*
 * Division by mbedtls_mpi: A = Q * B + R  (HAC 14.20)
 */
int Bigint::mbedtls::mpi_div_mpi( Bigint::Data *Q, Bigint::Data *R, const Bigint::Data *A, const Bigint::Data *B )
{
    int ret;
    size_t i, n, t, k;
    Bigint::Data X, Y, Z, T1, T2;

    if( mpi_cmp_int( B, 0 ) == 0 )
        return( BIGINT_ERR_MPI_DIVISION_BY_ZERO );

    mpi_init( &X ); mpi_init( &Y ); mpi_init( &Z );
    mpi_init( &T1 ); mpi_init( &T2 );

    if( mpi_cmp_abs( A, B ) < 0 )
    {
        if( Q != NULL ) BIGINT_MPI_CHK( mpi_lset( Q, 0 ) );
        if( R != NULL ) BIGINT_MPI_CHK( mpi_copy( R, A ) );
        return( 0 );
    }

    BIGINT_MPI_CHK( mpi_copy( &X, A ) );
    BIGINT_MPI_CHK( mpi_copy( &Y, B ) );
    X.s = Y.s = 1;

    BIGINT_MPI_CHK( mpi_grow( &Z, A->n + 2 ) );
    BIGINT_MPI_CHK( mpi_lset( &Z,  0 ) );
    BIGINT_MPI_CHK( mpi_grow( &T1, 2 ) );
    BIGINT_MPI_CHK( mpi_grow( &T2, 3 ) );

    k = mpi_bitlen( &Y ) % biL;
    if( k < biL - 1 )
    {
        k = biL - 1 - k;
        BIGINT_MPI_CHK( mpi_shift_l( &X, k ) );
        BIGINT_MPI_CHK( mpi_shift_l( &Y, k ) );
    }
    else k = 0;

    n = X.n - 1;
    t = Y.n - 1;
    BIGINT_MPI_CHK( mpi_shift_l( &Y, biL * ( n - t ) ) );

    while( mpi_cmp_mpi( &X, &Y ) >= 0 )
    {
        Z.p[n - t]++;
        BIGINT_MPI_CHK( mpi_sub_mpi( &X, &X, &Y ) );
    }
    BIGINT_MPI_CHK( mpi_shift_r( &Y, biL * ( n - t ) ) );

    for( i = n; i > t ; i-- )
    {
        if( X.p[i] >= Y.p[t] )
            Z.p[i - t - 1] = ~0;
        else
        {
            Z.p[i - t - 1] = mbedtls_int_div_int( X.p[i], X.p[i - 1],
                                                            Y.p[t], NULL);
        }

        Z.p[i - t - 1]++;
        do
        {
            Z.p[i - t - 1]--;

            BIGINT_MPI_CHK( mpi_lset( &T1, 0 ) );
            T1.p[0] = ( t < 1 ) ? 0 : Y.p[t - 1];
            T1.p[1] = Y.p[t];
            BIGINT_MPI_CHK( mpi_mul_int( &T1, &T1, Z.p[i - t - 1] ) );

            BIGINT_MPI_CHK( mpi_lset( &T2, 0 ) );
            T2.p[0] = ( i < 2 ) ? 0 : X.p[i - 2];
            T2.p[1] = ( i < 1 ) ? 0 : X.p[i - 1];
            T2.p[2] = X.p[i];
        }
        while( mpi_cmp_mpi( &T1, &T2 ) > 0 );

        BIGINT_MPI_CHK( mpi_mul_int( &T1, &Y, Z.p[i - t - 1] ) );
        BIGINT_MPI_CHK( mpi_shift_l( &T1,  biL * ( i - t - 1 ) ) );
        BIGINT_MPI_CHK( mpi_sub_mpi( &X, &X, &T1 ) );

        if( mpi_cmp_int( &X, 0 ) < 0 )
        {
            BIGINT_MPI_CHK( mpi_copy( &T1, &Y ) );
            BIGINT_MPI_CHK( mpi_shift_l( &T1, biL * ( i - t - 1 ) ) );
            BIGINT_MPI_CHK( mpi_add_mpi( &X, &X, &T1 ) );
            Z.p[i - t - 1]--;
        }
    }

    if( Q != NULL )
    {
        BIGINT_MPI_CHK( mpi_copy( Q, &Z ) );
        Q->s = A->s * B->s;
    }

    if( R != NULL )
    {
        BIGINT_MPI_CHK( mpi_shift_r( &X, k ) );
        X.s = A->s;
        BIGINT_MPI_CHK( mpi_copy( R, &X ) );

        if( mpi_cmp_int( R, 0 ) == 0 )
            R->s = 1;
    }

cleanup:

    mpi_free( &X ); mpi_free( &Y ); mpi_free( &Z );
    mpi_free( &T1 ); mpi_free( &T2 );

    return( ret );
}

/*
 * Division by int: A = Q * b + R
 */
int Bigint::mbedtls::mpi_div_int( Bigint::Data *Q, Bigint::Data *R, const Bigint::Data *A, bigint_sint_t b )
{
    Bigint::Data _B;
    bigint_uint_t p[1];

    p[0] = ( b < 0 ) ? -b : b;
    _B.s = ( b < 0 ) ? -1 : 1;
    _B.n = 1;
    _B.p = p;

    return( mpi_div_mpi( Q, R, A, &_B ) );
}

/*
 * Modulo: R = A mod B
 */
int Bigint::mbedtls::mpi_mod_mpi( Bigint::Data *R, const Bigint::Data *A, const Bigint::Data *B )
{
    int ret;

    if( mpi_cmp_int( B, 0 ) < 0 )
        return( BIGINT_ERR_MPI_NEGATIVE_VALUE );

    BIGINT_MPI_CHK( mpi_div_mpi( NULL, R, A, B ) );

    while( mpi_cmp_int( R, 0 ) < 0 )
      BIGINT_MPI_CHK( mpi_add_mpi( R, R, B ) );

    while( mpi_cmp_mpi( R, B ) >= 0 )
      BIGINT_MPI_CHK( mpi_sub_mpi( R, R, B ) );

cleanup:

    return( ret );
}

/*
 * Modulo: r = A mod b
 */
int Bigint::mbedtls::mpi_mod_int( bigint_uint_t *r, const Bigint::Data *A, bigint_sint_t b )
{
    size_t i;
    bigint_uint_t x, y, z;

    if( b == 0 )
        return( BIGINT_ERR_MPI_DIVISION_BY_ZERO );

    if( b < 0 )
        return( BIGINT_ERR_MPI_NEGATIVE_VALUE );

    /*
     * handle trivial cases
     */
    if( b == 1 )
    {
        *r = 0;
        return( 0 );
    }

    if( b == 2 )
    {
        *r = A->p[0] & 1;
        return( 0 );
    }

    /*
     * general case
     */
    for( i = A->n, y = 0; i > 0; i-- )
    {
        x  = A->p[i - 1];
        y  = ( y << biH ) | ( x >> biH );
        z  = y / b;
        y -= z * b;

        x <<= biH;
        y  = ( y << biH ) | ( x >> biH );
        z  = y / b;
        y -= z * b;
    }

    /*
     * If A is negative, then the current y represents a negative value.
     * Flipping it to the positive side.
     */
    if( A->s < 0 && y != 0 )
        y = b - y;

    *r = y;

    return( 0 );
}

#if defined(FULL_BIGINT_LIB)
/*
 * Fast Montgomery initialization (thanks to Tom St Denis)
 */
void Bigint::mbedtls::mpi_montg_init( bigint_uint_t *mm, const Bigint::Data *N )
{
    bigint_uint_t x, m0 = N->p[0];
    unsigned int i;

    x  = m0;
    x += ( ( m0 + 2 ) & 4 ) << 1;

    for( i = biL; i >= 8; i /= 2 )
        x *= ( 2 - ( m0 * x ) );

    *mm = ~x + 1;
}

/*
 * Montgomery multiplication: A = A * B * R^-1 mod N  (HAC 14.36)
 */
int Bigint::mbedtls::mpi_montmul( Bigint::Data *A, const Bigint::Data *B, const Bigint::Data *N, bigint_uint_t mm,
                         const Bigint::Data *T )
{
    size_t i, n, m;
    bigint_uint_t u0, u1, *d;

    if( T->n < N->n + 1 || T->p == NULL )
        return( BIGINT_ERR_MPI_BAD_INPUT_DATA );

    memset( T->p, 0, T->n * ciL );

    d = T->p;
    n = N->n;
    m = ( B->n < n ) ? B->n : n;

    for( i = 0; i < n; i++ )
    {
        /*
         * T = (T + u0*B + u1*N) / 2^biL
         */
        u0 = A->p[i];
        u1 = ( d[0] + u0 * B->p[0] ) * mm;

        mpi_mul_hlp( m, B->p, d, u0 );
        mpi_mul_hlp( n, N->p, d, u1 );

        *d++ = u0; d[n + 1] = 0;
    }

    memcpy( A->p, d, ( n + 1 ) * ciL );

    if( mpi_cmp_abs( A, N ) >= 0 )
        mpi_sub_hlp( n, N->p, A->p );
    else
        /* prevent timing attacks */
        mpi_sub_hlp( n, A->p, T->p );

    return( 0 );
}

/*
 * Montgomery reduction: A = A * R^-1 mod N
 */
int Bigint::mbedtls::mpi_montred( Bigint::Data *A, const Bigint::Data *N, bigint_uint_t mm, const Bigint::Data *T )
{
    bigint_uint_t z = 1;
    Bigint::Data U;

    U.n = U.s = (int) z;
    U.p = &z;

    return( mpi_montmul( A, &U, N, mm, T ) );
}

/*
 * Sliding-window exponentiation: X = A^E mod N  (HAC 14.85)
 */
int Bigint::mbedtls::mpi_exp_mod( Bigint::Data *X, const Bigint::Data *A, const Bigint::Data *E, const Bigint::Data *N, Bigint::Data *_RR )
{
    int ret;
    size_t wbits, wsize, one = 1;
    size_t i, j, nblimbs;
    size_t bufsize, nbits;
    bigint_uint_t ei, mm, state;
    Bigint::Data RR, T, W[ 2 << BIGINT_MPI_WINDOW_SIZE ], Apos;
    int neg;

    if( mpi_cmp_int( N, 0 ) < 0 || ( N->p[0] & 1 ) == 0 )
        return( BIGINT_ERR_MPI_BAD_INPUT_DATA );

    if( mpi_cmp_int( E, 0 ) < 0 )
        return( BIGINT_ERR_MPI_BAD_INPUT_DATA );

    /*
     * Init temps and window size
     */
    mpi_montg_init( &mm, N );
    mpi_init( &RR ); mpi_init( &T );
    mpi_init( &Apos );
    memset( W, 0, sizeof( W ) );

    i = mpi_bitlen( E );

    wsize = ( i > 671 ) ? 6 : ( i > 239 ) ? 5 :
            ( i >  79 ) ? 4 : ( i >  23 ) ? 3 : 1;

    if( wsize > BIGINT_MPI_WINDOW_SIZE )
        wsize = BIGINT_MPI_WINDOW_SIZE;

    j = N->n + 1;
    BIGINT_MPI_CHK( mpi_grow( X, j ) );
    BIGINT_MPI_CHK( mpi_grow( &W[1],  j ) );
    BIGINT_MPI_CHK( mpi_grow( &T, j * 2 ) );

    /*
     * Compensate for negative A (and correct at the end)
     */
    neg = ( A->s == -1 );
    if( neg )
    {
        BIGINT_MPI_CHK( mpi_copy( &Apos, A ) );
        Apos.s = 1;
        A = &Apos;
    }

    /*
     * If 1st call, pre-compute R^2 mod N
     */
    if( _RR == NULL || _RR->p == NULL )
    {
        BIGINT_MPI_CHK( mpi_lset( &RR, 1 ) );
        BIGINT_MPI_CHK( mpi_shift_l( &RR, N->n * 2 * biL ) );
        BIGINT_MPI_CHK( mpi_mod_mpi( &RR, &RR, N ) );

        if( _RR != NULL )
            memcpy( _RR, &RR, sizeof( Bigint::Data ) );
    }
    else
        memcpy( &RR, _RR, sizeof( Bigint::Data ) );

    /*
     * W[1] = A * R^2 * R^-1 mod N = A * R mod N
     */
    if( mpi_cmp_mpi( A, N ) >= 0 )
        BIGINT_MPI_CHK( mpi_mod_mpi( &W[1], A, N ) );
    else
        BIGINT_MPI_CHK( mpi_copy( &W[1], A ) );

    BIGINT_MPI_CHK( mpi_montmul( &W[1], &RR, N, mm, &T ) );

    /*
     * X = R^2 * R^-1 mod N = R mod N
     */
    BIGINT_MPI_CHK( mpi_copy( X, &RR ) );
    BIGINT_MPI_CHK( mpi_montred( X, N, mm, &T ) );

    if( wsize > 1 )
    {
        /*
         * W[1 << (wsize - 1)] = W[1] ^ (wsize - 1)
         */
        j =  one << ( wsize - 1 );

        BIGINT_MPI_CHK( mpi_grow( &W[j], N->n + 1 ) );
        BIGINT_MPI_CHK( mpi_copy( &W[j], &W[1]    ) );

        for( i = 0; i < wsize - 1; i++ )
            BIGINT_MPI_CHK( mpi_montmul( &W[j], &W[j], N, mm, &T ) );

        /*
         * W[i] = W[i - 1] * W[1]
         */
        for( i = j + 1; i < ( one << wsize ); i++ )
        {
            BIGINT_MPI_CHK( mpi_grow( &W[i], N->n + 1 ) );
            BIGINT_MPI_CHK( mpi_copy( &W[i], &W[i - 1] ) );

            BIGINT_MPI_CHK( mpi_montmul( &W[i], &W[1], N, mm, &T ) );
        }
    }

    nblimbs = E->n;
    bufsize = 0;
    nbits   = 0;
    wbits   = 0;
    state   = 0;

    while( 1 )
    {
        if( bufsize == 0 )
        {
            if( nblimbs == 0 )
                break;

            nblimbs--;

            bufsize = sizeof( bigint_uint_t ) << 3;
        }

        bufsize--;

        ei = (E->p[nblimbs] >> bufsize) & 1;

        /*
         * skip leading 0s
         */
        if( ei == 0 && state == 0 )
            continue;

        if( ei == 0 && state == 1 )
        {
            /*
             * out of window, square X
             */
            BIGINT_MPI_CHK( mpi_montmul( X, X, N, mm, &T ) );
            continue;
        }

        /*
         * add ei to current window
         */
        state = 2;

        nbits++;
        wbits |= ( ei << ( wsize - nbits ) );

        if( nbits == wsize )
        {
            /*
             * X = X^wsize R^-1 mod N
             */
            for( i = 0; i < wsize; i++ )
                BIGINT_MPI_CHK( mpi_montmul( X, X, N, mm, &T ) );

            /*
             * X = X * W[wbits] R^-1 mod N
             */
            BIGINT_MPI_CHK( mpi_montmul( X, &W[wbits], N, mm, &T ) );

            state--;
            nbits = 0;
            wbits = 0;
        }
    }

    /*
     * process the remaining bits
     */
    for( i = 0; i < nbits; i++ )
    {
        BIGINT_MPI_CHK( mpi_montmul( X, X, N, mm, &T ) );

        wbits <<= 1;

        if( ( wbits & ( one << wsize ) ) != 0 )
            BIGINT_MPI_CHK( mpi_montmul( X, &W[1], N, mm, &T ) );
    }

    /*
     * X = A^E * R * R^-1 mod N = A^E mod N
     */
    BIGINT_MPI_CHK( mpi_montred( X, N, mm, &T ) );

    if( neg )
    {
        X->s = -1;
        BIGINT_MPI_CHK( mpi_add_mpi( X, N, X ) );
    }

cleanup:

    for( i = ( one << ( wsize - 1 ) ); i < ( one << wsize ); i++ )
        mpi_free( &W[i] );

    mpi_free( &W[1] ); mpi_free( &T ); mpi_free( &Apos );

    if( _RR == NULL || _RR->p == NULL )
        mpi_free( &RR );

    return( ret );
}

/*
 * Greatest common divisor: G = gcd(A, B)  (HAC 14.54)
 */
int Bigint::mbedtls::mpi_gcd( Bigint::Data *G, const Bigint::Data *A, const Bigint::Data *B )
{
    int ret;
    size_t lz, lzt;
    Bigint::Data TG, TA, TB;

    mpi_init( &TG ); mpi_init( &TA ); mpi_init( &TB );

    BIGINT_MPI_CHK( mpi_copy( &TA, A ) );
    BIGINT_MPI_CHK( mpi_copy( &TB, B ) );

    lz = mpi_lsb( &TA );
    lzt = mpi_lsb( &TB );

    if( lzt < lz )
        lz = lzt;

    BIGINT_MPI_CHK( mpi_shift_r( &TA, lz ) );
    BIGINT_MPI_CHK( mpi_shift_r( &TB, lz ) );

    TA.s = TB.s = 1;

    while( mpi_cmp_int( &TA, 0 ) != 0 )
    {
        BIGINT_MPI_CHK( mpi_shift_r( &TA, mpi_lsb( &TA ) ) );
        BIGINT_MPI_CHK( mpi_shift_r( &TB, mpi_lsb( &TB ) ) );

        if( mpi_cmp_mpi( &TA, &TB ) >= 0 )
        {
            BIGINT_MPI_CHK( mpi_sub_abs( &TA, &TA, &TB ) );
            BIGINT_MPI_CHK( mpi_shift_r( &TA, 1 ) );
        }
        else
        {
            BIGINT_MPI_CHK( mpi_sub_abs( &TB, &TB, &TA ) );
            BIGINT_MPI_CHK( mpi_shift_r( &TB, 1 ) );
        }
    }

    BIGINT_MPI_CHK( mpi_shift_l( &TB, lz ) );
    BIGINT_MPI_CHK( mpi_copy( G, &TB ) );

cleanup:

    mpi_free( &TG ); mpi_free( &TA ); mpi_free( &TB );

    return( ret );
}

/*
 * Fill X with size bytes of random.
 *
 * Use a temporary bytes representation to make sure the result is the same
 * regardless of the platform endianness (useful when f_rng is actually
 * deterministic, eg for tests).
 */
int Bigint::mbedtls::mpi_fill_random( Bigint::Data *X, size_t size,
                     int (*f_rng)(void *, unsigned char *, size_t),
                     void *p_rng )
{
    int ret;
    unsigned char buf[BIGINT_MPI_MAX_SIZE];

    if( size > BIGINT_MPI_MAX_SIZE )
        return( BIGINT_ERR_MPI_BAD_INPUT_DATA );

    BIGINT_MPI_CHK( f_rng( p_rng, buf, size ) );
    BIGINT_MPI_CHK( mpi_read_binary( X, buf, size ) );

cleanup:
    return( ret );
}

/*
 * Modular inverse: X = A^-1 mod N  (HAC 14.61 / 14.64)
 */
int Bigint::mbedtls::mpi_inv_mod( Bigint::Data *X, const Bigint::Data *A, const Bigint::Data *N )
{
    int ret;
    Bigint::Data G, TA, TU, U1, U2, TB, TV, V1, V2;

    if( mpi_cmp_int( N, 0 ) <= 0 )
        return( BIGINT_ERR_MPI_BAD_INPUT_DATA );

    mpi_init( &TA ); mpi_init( &TU ); mpi_init( &U1 ); mpi_init( &U2 );
    mpi_init( &G ); mpi_init( &TB ); mpi_init( &TV );
    mpi_init( &V1 ); mpi_init( &V2 );

    BIGINT_MPI_CHK( mpi_gcd( &G, A, N ) );

    if( mpi_cmp_int( &G, 1 ) != 0 )
    {
        ret = BIGINT_ERR_MPI_NOT_ACCEPTABLE;
        goto cleanup;
    }

    BIGINT_MPI_CHK( mpi_mod_mpi( &TA, A, N ) );
    BIGINT_MPI_CHK( mpi_copy( &TU, &TA ) );
    BIGINT_MPI_CHK( mpi_copy( &TB, N ) );
    BIGINT_MPI_CHK( mpi_copy( &TV, N ) );

    BIGINT_MPI_CHK( mpi_lset( &U1, 1 ) );
    BIGINT_MPI_CHK( mpi_lset( &U2, 0 ) );
    BIGINT_MPI_CHK( mpi_lset( &V1, 0 ) );
    BIGINT_MPI_CHK( mpi_lset( &V2, 1 ) );

    do
    {
        while( ( TU.p[0] & 1 ) == 0 )
        {
            BIGINT_MPI_CHK( mpi_shift_r( &TU, 1 ) );

            if( ( U1.p[0] & 1 ) != 0 || ( U2.p[0] & 1 ) != 0 )
            {
                BIGINT_MPI_CHK( mpi_add_mpi( &U1, &U1, &TB ) );
                BIGINT_MPI_CHK( mpi_sub_mpi( &U2, &U2, &TA ) );
            }

            BIGINT_MPI_CHK( mpi_shift_r( &U1, 1 ) );
            BIGINT_MPI_CHK( mpi_shift_r( &U2, 1 ) );
        }

        while( ( TV.p[0] & 1 ) == 0 )
        {
            BIGINT_MPI_CHK( mpi_shift_r( &TV, 1 ) );

            if( ( V1.p[0] & 1 ) != 0 || ( V2.p[0] & 1 ) != 0 )
            {
                BIGINT_MPI_CHK( mpi_add_mpi( &V1, &V1, &TB ) );
                BIGINT_MPI_CHK( mpi_sub_mpi( &V2, &V2, &TA ) );
            }

            BIGINT_MPI_CHK( mpi_shift_r( &V1, 1 ) );
            BIGINT_MPI_CHK( mpi_shift_r( &V2, 1 ) );
        }

        if( mpi_cmp_mpi( &TU, &TV ) >= 0 )
        {
            BIGINT_MPI_CHK( mpi_sub_mpi( &TU, &TU, &TV ) );
            BIGINT_MPI_CHK( mpi_sub_mpi( &U1, &U1, &V1 ) );
            BIGINT_MPI_CHK( mpi_sub_mpi( &U2, &U2, &V2 ) );
        }
        else
        {
            BIGINT_MPI_CHK( mpi_sub_mpi( &TV, &TV, &TU ) );
            BIGINT_MPI_CHK( mpi_sub_mpi( &V1, &V1, &U1 ) );
            BIGINT_MPI_CHK( mpi_sub_mpi( &V2, &V2, &U2 ) );
        }
    }
    while( mpi_cmp_int( &TU, 0 ) != 0 );

    while( mpi_cmp_int( &V1, 0 ) < 0 )
        BIGINT_MPI_CHK( mpi_add_mpi( &V1, &V1, N ) );

    while( mpi_cmp_mpi( &V1, N ) >= 0 )
        BIGINT_MPI_CHK( mpi_sub_mpi( &V1, &V1, N ) );

    BIGINT_MPI_CHK( mpi_copy( X, &V1 ) );

cleanup:

    mpi_free( &TA ); mpi_free( &TU ); mpi_free( &U1 ); mpi_free( &U2 );
    mpi_free( &G ); mpi_free( &TB ); mpi_free( &TV );
    mpi_free( &V1 ); mpi_free( &V2 );

    return( ret );
}



static const int small_prime[] =
{
        3,    5,    7,   11,   13,   17,   19,   23,
       29,   31,   37,   41,   43,   47,   53,   59,
       61,   67,   71,   73,   79,   83,   89,   97,
      101,  103,  107,  109,  113,  127,  131,  137,
      139,  149,  151,  157,  163,  167,  173,  179,
      181,  191,  193,  197,  199,  211,  223,  227,
      229,  233,  239,  241,  251,  257,  263,  269,
      271,  277,  281,  283,  293,  307,  311,  313,
      317,  331,  337,  347,  349,  353,  359,  367,
      373,  379,  383,  389,  397,  401,  409,  419,
      421,  431,  433,  439,  443,  449,  457,  461,
      463,  467,  479,  487,  491,  499,  503,  509,
      521,  523,  541,  547,  557,  563,  569,  571,
      577,  587,  593,  599,  601,  607,  613,  617,
      619,  631,  641,  643,  647,  653,  659,  661,
      673,  677,  683,  691,  701,  709,  719,  727,
      733,  739,  743,  751,  757,  761,  769,  773,
      787,  797,  809,  811,  821,  823,  827,  829,
      839,  853,  857,  859,  863,  877,  881,  883,
      887,  907,  911,  919,  929,  937,  941,  947,
      953,  967,  971,  977,  983,  991,  997, -103
};

/*
 * Small divisors test (X must be positive)
 *
 * Return values:
 * 0: no small factor (possible prime, more tests needed)
 * 1: certain prime
 * BIGINT_ERR_MPI_NOT_ACCEPTABLE: certain non-prime
 * other negative: error
 */
int Bigint::mbedtls::mpi_check_small_factors( const Bigint::Data *X )
{
    int ret = 0;
    size_t i;
    bigint_uint_t r;

    if( ( X->p[0] & 1 ) == 0 )
        return( BIGINT_ERR_MPI_NOT_ACCEPTABLE );

    for( i = 0; small_prime[i] > 0; i++ )
    {
        if( mpi_cmp_int( X, small_prime[i] ) <= 0 )
            return( 1 );

        BIGINT_MPI_CHK( mpi_mod_int( &r, X, small_prime[i] ) );

        if( r == 0 )
            return( BIGINT_ERR_MPI_NOT_ACCEPTABLE );
    }

cleanup:
    return( ret );
}

/*
 * Miller-Rabin pseudo-primality test  (HAC 4.24)
 */
int Bigint::mbedtls::mpi_miller_rabin( const Bigint::Data *X,
                             int (*f_rng)(void *, unsigned char *, size_t),
                             void *p_rng )
{
    int ret, count;
    size_t i, j, k, n, s;
    Bigint::Data W, R, T, A, RR;

    mpi_init( &W ); mpi_init( &R ); mpi_init( &T ); mpi_init( &A );
    mpi_init( &RR );

    /*
     * W = |X| - 1
     * R = W >> lsb( W )
     */
    BIGINT_MPI_CHK( mpi_sub_int( &W, X, 1 ) );
    s = mpi_lsb( &W );
    BIGINT_MPI_CHK( mpi_copy( &R, &W ) );
    BIGINT_MPI_CHK( mpi_shift_r( &R, s ) );

    i = mpi_bitlen( X );
    /*
     * HAC, table 4.4
     */
    n = ( ( i >= 1300 ) ?  2 : ( i >=  850 ) ?  3 :
          ( i >=  650 ) ?  4 : ( i >=  350 ) ?  8 :
          ( i >=  250 ) ? 12 : ( i >=  150 ) ? 18 : 27 );

    for( i = 0; i < n; i++ )
    {
        /*
         * pick a random A, 1 < A < |X| - 1
         */
        BIGINT_MPI_CHK( mpi_fill_random( &A, X->n * ciL, f_rng, p_rng ) );

        if( mpi_cmp_mpi( &A, &W ) >= 0 )
        {
            j = mpi_bitlen( &A ) - mpi_bitlen( &W );
            BIGINT_MPI_CHK( mpi_shift_r( &A, j + 1 ) );
        }
        A.p[0] |= 3;

        count = 0;
        do {
            BIGINT_MPI_CHK( mpi_fill_random( &A, X->n * ciL, f_rng, p_rng ) );

            j = mpi_bitlen( &A );
            k = mpi_bitlen( &W );
            if (j > k) {
                BIGINT_MPI_CHK( mpi_shift_r( &A, j - k ) );
            }

            if (count++ > 30) {
                return BIGINT_ERR_MPI_NOT_ACCEPTABLE;
            }

        } while ( mpi_cmp_mpi( &A, &W ) >= 0 ||
                  mpi_cmp_int( &A, 1 )  <= 0    );

        /*
         * A = A^R mod |X|
         */
        BIGINT_MPI_CHK( mpi_exp_mod( &A, &A, &R, X, &RR ) );

        if( mpi_cmp_mpi( &A, &W ) == 0 ||
            mpi_cmp_int( &A,  1 ) == 0 )
            continue;

        j = 1;
        while( j < s && mpi_cmp_mpi( &A, &W ) != 0 )
        {
            /*
             * A = A * A mod |X|
             */
            BIGINT_MPI_CHK( mpi_mul_mpi( &T, &A, &A ) );
            BIGINT_MPI_CHK( mpi_mod_mpi( &A, &T, X  ) );

            if( mpi_cmp_int( &A, 1 ) == 0 )
                break;

            j++;
        }

        /*
         * not prime if A != |X| - 1 or A == 1
         */
        if( mpi_cmp_mpi( &A, &W ) != 0 ||
            mpi_cmp_int( &A,  1 ) == 0 )
        {
            ret = BIGINT_ERR_MPI_NOT_ACCEPTABLE;
            break;
        }
    }

cleanup:
    mpi_free( &W ); mpi_free( &R ); mpi_free( &T ); mpi_free( &A );
    mpi_free( &RR );

    return( ret );
}

/*
 * Pseudo-primality test: small factors, then Miller-Rabin
 */
int Bigint::mbedtls::mpi_is_prime( const Bigint::Data *X,
                  int (*f_rng)(void *, unsigned char *, size_t),
                  void *p_rng )
{
    int ret;
    Bigint::Data XX;

    XX.s = 1;
    XX.n = X->n;
    XX.p = X->p;

    if( mpi_cmp_int( &XX, 0 ) == 0 ||
        mpi_cmp_int( &XX, 1 ) == 0 )
        return( BIGINT_ERR_MPI_NOT_ACCEPTABLE );

    if( mpi_cmp_int( &XX, 2 ) == 0 )
        return( 0 );

    if( ( ret = mpi_check_small_factors( &XX ) ) != 0 )
    {
        if( ret == 1 )
            return( 0 );

        return( ret );
    }

    return( mpi_miller_rabin( &XX, f_rng, p_rng ) );
}

/*
 * Prime number generation
 */
int Bigint::mbedtls::mpi_gen_prime( Bigint::Data *X, size_t nbits, int dh_flag,
                   int (*f_rng)(void *, unsigned char *, size_t),
                   void *p_rng )
{
    int ret;
    size_t k, n;
    bigint_uint_t r;
    Bigint::Data Y;

    if( nbits < 3 || nbits > BIGINT_MPI_MAX_BITS )
        return( BIGINT_ERR_MPI_BAD_INPUT_DATA );

    mpi_init( &Y );

    n = BITS_TO_LIMBS( nbits );

    BIGINT_MPI_CHK( mpi_fill_random( X, n * ciL, f_rng, p_rng ) );

    k = mpi_bitlen( X );
    if( k > nbits ) BIGINT_MPI_CHK( mpi_shift_r( X, k - nbits + 1 ) );

    mpi_set_bit( X, nbits-1, 1 );

    X->p[0] |= 1;

    if( dh_flag == 0 )
    {
        while( ( ret = mpi_is_prime( X, f_rng, p_rng ) ) != 0 )
        {
            if( ret != BIGINT_ERR_MPI_NOT_ACCEPTABLE )
                goto cleanup;

            BIGINT_MPI_CHK( mpi_add_int( X, X, 2 ) );
        }
    }
    else
    {
        /*
         * An necessary condition for Y and X = 2Y + 1 to be prime
         * is X = 2 mod 3 (which is equivalent to Y = 2 mod 3).
         * Make sure it is satisfied, while keeping X = 3 mod 4
         */

        X->p[0] |= 2;

        BIGINT_MPI_CHK( mpi_mod_int( &r, X, 3 ) );
        if( r == 0 )
            BIGINT_MPI_CHK( mpi_add_int( X, X, 8 ) );
        else if( r == 1 )
            BIGINT_MPI_CHK( mpi_add_int( X, X, 4 ) );

        /* Set Y = (X-1) / 2, which is X / 2 because X is odd */
        BIGINT_MPI_CHK( mpi_copy( &Y, X ) );
        BIGINT_MPI_CHK( mpi_shift_r( &Y, 1 ) );

        while( 1 )
        {
            /*
             * First, check small factors for X and Y
             * before doing Miller-Rabin on any of them
             */
            if( ( ret = mpi_check_small_factors(  X         ) ) == 0 &&
                ( ret = mpi_check_small_factors( &Y         ) ) == 0 &&
                ( ret = mpi_miller_rabin(  X, f_rng, p_rng  ) ) == 0 &&
                ( ret = mpi_miller_rabin( &Y, f_rng, p_rng  ) ) == 0 )
            {
                break;
            }

            if( ret != BIGINT_ERR_MPI_NOT_ACCEPTABLE )
                goto cleanup;

            /*
             * Next candidates. We want to preserve Y = (X-1) / 2 and
             * Y = 1 mod 2 and Y = 2 mod 3 (eq X = 3 mod 4 and X = 2 mod 3)
             * so up Y by 6 and X by 12.
             */
            BIGINT_MPI_CHK( mpi_add_int(  X,  X, 12 ) );
            BIGINT_MPI_CHK( mpi_add_int( &Y, &Y, 6  ) );
        }
    }

cleanup:

    mpi_free( &Y );

    return( ret );
}

#endif /* FULL_BIGINT_LIB */

#if defined(BIGINT_SELF_TEST)

#define GCD_PAIR_COUNT  3

static const int gcd_pairs[GCD_PAIR_COUNT][3] =
{
    { 693, 609, 21 },
    { 1764, 868, 28 },
    { 768454923, 542167814, 1 }
};

/*
 * Checkup routine
 */
int mbedtls_mpi_self_test( int verbose )
{
    int ret, i;
    mbedtls_mpi A, E, N, X, Y, U, V;

    mbedtls_mpi_init( &A ); mbedtls_mpi_init( &E ); mbedtls_mpi_init( &N ); mbedtls_mpi_init( &X );
    mbedtls_mpi_init( &Y ); mbedtls_mpi_init( &U ); mbedtls_mpi_init( &V );

    BIGINT_MPI_CHK( mbedtls_mpi_read_string( &A, 16,
        "EFE021C2645FD1DC586E69184AF4A31E" \
        "D5F53E93B5F123FA41680867BA110131" \
        "944FE7952E2517337780CB0DB80E61AA" \
        "E7C8DDC6C5C6AADEB34EB38A2F40D5E6" ) );

    BIGINT_MPI_CHK( mbedtls_mpi_read_string( &E, 16,
        "B2E7EFD37075B9F03FF989C7C5051C20" \
        "34D2A323810251127E7BF8625A4F49A5" \
        "F3E27F4DA8BD59C47D6DAABA4C8127BD" \
        "5B5C25763222FEFCCFC38B832366C29E" ) );

    BIGINT_MPI_CHK( mbedtls_mpi_read_string( &N, 16,
        "0066A198186C18C10B2F5ED9B522752A" \
        "9830B69916E535C8F047518A889A43A5" \
        "94B6BED27A168D31D4A52F88925AA8F5" ) );

    BIGINT_MPI_CHK( mbedtls_mpi_mul_mpi( &X, &A, &N ) );

    BIGINT_MPI_CHK( mbedtls_mpi_read_string( &U, 16,
        "602AB7ECA597A3D6B56FF9829A5E8B85" \
        "9E857EA95A03512E2BAE7391688D264A" \
        "A5663B0341DB9CCFD2C4C5F421FEC814" \
        "8001B72E848A38CAE1C65F78E56ABDEF" \
        "E12D3C039B8A02D6BE593F0BBBDA56F1" \
        "ECF677152EF804370C1A305CAF3B5BF1" \
        "30879B56C61DE584A0F53A2447A51E" ) );

    if( verbose != 0 )
        mbedtls_printf( "  MPI test #1 (mul_mpi): " );

    if( mbedtls_mpi_cmp_mpi( &X, &U ) != 0 )
    {
        if( verbose != 0 )
            mbedtls_printf( "failed\n" );

        ret = 1;
        goto cleanup;
    }

    if( verbose != 0 )
        mbedtls_printf( "passed\n" );

    BIGINT_MPI_CHK( mbedtls_mpi_div_mpi( &X, &Y, &A, &N ) );

    BIGINT_MPI_CHK( mbedtls_mpi_read_string( &U, 16,
        "256567336059E52CAE22925474705F39A94" ) );

    BIGINT_MPI_CHK( mbedtls_mpi_read_string( &V, 16,
        "6613F26162223DF488E9CD48CC132C7A" \
        "0AC93C701B001B092E4E5B9F73BCD27B" \
        "9EE50D0657C77F374E903CDFA4C642" ) );

    if( verbose != 0 )
        mbedtls_printf( "  MPI test #2 (div_mpi): " );

    if( mbedtls_mpi_cmp_mpi( &X, &U ) != 0 ||
        mbedtls_mpi_cmp_mpi( &Y, &V ) != 0 )
    {
        if( verbose != 0 )
            mbedtls_printf( "failed\n" );

        ret = 1;
        goto cleanup;
    }

    if( verbose != 0 )
        mbedtls_printf( "passed\n" );

    BIGINT_MPI_CHK( mbedtls_mpi_exp_mod( &X, &A, &E, &N, NULL ) );

    BIGINT_MPI_CHK( mbedtls_mpi_read_string( &U, 16,
        "36E139AEA55215609D2816998ED020BB" \
        "BD96C37890F65171D948E9BC7CBAA4D9" \
        "325D24D6A3C12710F10A09FA08AB87" ) );

    if( verbose != 0 )
        mbedtls_printf( "  MPI test #3 (exp_mod): " );

    if( mbedtls_mpi_cmp_mpi( &X, &U ) != 0 )
    {
        if( verbose != 0 )
            mbedtls_printf( "failed\n" );

        ret = 1;
        goto cleanup;
    }

    if( verbose != 0 )
        mbedtls_printf( "passed\n" );

    BIGINT_MPI_CHK( mbedtls_mpi_inv_mod( &X, &A, &N ) );

    BIGINT_MPI_CHK( mbedtls_mpi_read_string( &U, 16,
        "003A0AAEDD7E784FC07D8F9EC6E3BFD5" \
        "C3DBA76456363A10869622EAC2DD84EC" \
        "C5B8A74DAC4D09E03B5E0BE779F2DF61" ) );

    if( verbose != 0 )
        mbedtls_printf( "  MPI test #4 (inv_mod): " );

    if( mbedtls_mpi_cmp_mpi( &X, &U ) != 0 )
    {
        if( verbose != 0 )
            mbedtls_printf( "failed\n" );

        ret = 1;
        goto cleanup;
    }

    if( verbose != 0 )
        mbedtls_printf( "passed\n" );

    if( verbose != 0 )
        mbedtls_printf( "  MPI test #5 (simple gcd): " );

    for( i = 0; i < GCD_PAIR_COUNT; i++ )
    {
        BIGINT_MPI_CHK( mbedtls_mpi_lset( &X, gcd_pairs[i][0] ) );
        BIGINT_MPI_CHK( mbedtls_mpi_lset( &Y, gcd_pairs[i][1] ) );

        BIGINT_MPI_CHK( mbedtls_mpi_gcd( &A, &X, &Y ) );

        if( mbedtls_mpi_cmp_int( &A, gcd_pairs[i][2] ) != 0 )
        {
            if( verbose != 0 )
                mbedtls_printf( "failed at %d\n", i );

            ret = 1;
            goto cleanup;
        }
    }

    if( verbose != 0 )
        mbedtls_printf( "passed\n" );

cleanup:

    if( ret != 0 && verbose != 0 )
        mbedtls_printf( "Unexpected error, return code = %08X\n", ret );

    mbedtls_mpi_free( &A ); mbedtls_mpi_free( &E ); mbedtls_mpi_free( &N ); mbedtls_mpi_free( &X );
    mbedtls_mpi_free( &Y ); mbedtls_mpi_free( &U ); mbedtls_mpi_free( &V );

    if( verbose != 0 )
        mbedtls_printf( "\n" );

    return( ret );
}

#endif /* BIGINT_SELF_TEST */

/*****************************************************************************
 * Helper floating functions
 *****************************************************************************/
static int throw_if_failed(int x)
{
  switch (x)
  {
  case BIGINT_ERR_MPI_BAD_INPUT_DATA:
    throw std::invalid_argument("Bad input parameters to function.");
  case BIGINT_ERR_MPI_INVALID_CHARACTER:
    throw std::invalid_argument("There is an invalid character in the digit string.");
  case BIGINT_ERR_MPI_BUFFER_TOO_SMALL:
    throw std::underflow_error("The buffer is too small to write to.");
  case BIGINT_ERR_MPI_NEGATIVE_VALUE:
    throw std::invalid_argument("The input arguments are negative or result in illegal output.");
  case BIGINT_ERR_MPI_DIVISION_BY_ZERO:
    throw std::invalid_argument("The input argument for division is zero, which is not allowed.");
  case BIGINT_ERR_MPI_NOT_ACCEPTABLE:
    throw std::invalid_argument("The input arguments are not acceptable.");
  case BIGINT_ERR_MPI_ALLOC_FAILED:
    throw std::bad_alloc();
  default:
    if (x < 0)
      throw std::runtime_error("Unexpected mpi error");
  }
  return x;
}

/*****************************************************************************
 * Implemenetation of Bigint class
 *****************************************************************************/

//-----------------------------------------------------------------------------
// Constructs an #isNull object
Bigint::Bigint()
{
  mbedtls::mpi_init(&m_data);
}

//-----------------------------------------------------------------------------
// Constructs a #Bigint from an interger
Bigint::Bigint(bigint_sint_t i)
{
  mbedtls::mpi_init(&m_data);
  throw_if_failed(mbedtls::mpi_lset(&m_data, i));
}

//-----------------------------------------------------------------------------
// Constructs a #Bigint from the string representation
Bigint::Bigint(const char* pText,  int radix)
{
  mbedtls::mpi_init(&m_data);

  size_t sLength = pText ? strlen(pText) : 0;

  if (sLength)
    throw_if_failed(mbedtls::mpi_read_string(&m_data, radix, pText, sLength));
}

//-----------------------------------------------------------------------------
// Constructs a #Bigint from the string representation
Bigint::Bigint(const char* pText, size_t cbText, int radix)
{
  mbedtls::mpi_init(&m_data);

  size_t sLength = (pText && !cbText) ? strlen(pText) : cbText;

  if (sLength)
    throw_if_failed(mbedtls::mpi_read_string(&m_data, radix, pText, sLength));
}

//-----------------------------------------------------------------------------
// Constructs a #Bigint from the string representation
Bigint::Bigint(const std::string& s, int radix)
{
  mbedtls::mpi_init(&m_data);

  if (!s.empty())
    throw_if_failed(mbedtls::mpi_read_string(&m_data, radix, s.data(), s.size()));
}

#ifdef QT_CORE_LIB
//-----------------------------------------------------------------------------
// Constructs a #Bigint from the string representation
Bigint::Bigint(const QString& s, int radix)
{
  mbedtls::mpi_init(&m_data);

  if (!s.isEmpty())
    throw_if_failed(mbedtls::mpi_read_string(&m_data, radix, s.data(), static_cast<size_t>(s.size())));
}

//-----------------------------------------------------------------------------
// Constructs a $Bigint from the bit representation
Bigint::Bigint(const QBitArray& v)
{
  mbedtls::mpi_init(&m_data);

  for (int n = 0; n < v.size(); ++n)
    throw_if_failed(mbedtls::mpi_set_bit(&m_data, static_cast<size_t>(n), v.testBit(n) ? 1 : 0));
}

#endif

//-----------------------------------------------------------------------------
Bigint::Bigint(const Bigint& v)
{
  mbedtls::mpi_init(&m_data);
  mbedtls::mpi_copy(&m_data, &v.m_data);
}

//-----------------------------------------------------------------------------
Bigint::Bigint(Bigint&& v)
{
  mbedtls::mpi_init(&m_data);
  swap(v);
}

//-----------------------------------------------------------------------------
Bigint& Bigint::operator =(const Bigint& v)
{
  mbedtls::mpi_copy(&m_data, &v.m_data);
  return *this;
}

//-----------------------------------------------------------------------------
Bigint& Bigint::operator =(Bigint&& v)
{
  swap(v);
  return *this;
}

//-----------------------------------------------------------------------------
// Swaps content of the two classes
void Bigint::swap(Bigint& v)
{
  std::swap(m_data.s, v.m_data.s);
  std::swap(m_data.n, v.m_data.n);
  std::swap(m_data.p, v.m_data.p);
}


//-----------------------------------------------------------------------------
// Provides hashing for std::unordered_map and std::unordered_set
size_t Bigint::hash() const
{
  size_t rv = 0;
  for (bigint_uint_t* ptr = m_data.p, *pstop = m_data.p + m_data.n; ptr < pstop; ++ptr)
    rv ^= std::hash<bigint_uint_t>()(*ptr);

  return rv;
}

//-----------------------------------------------------------------------------
// Dtor
Bigint::~Bigint()
{
  mbedtls::mpi_free(&m_data);
}

//-----------------------------------------------------------------------------
// Returns true if this class #isNull or contains a value of zero
bool Bigint::isEmpty() const
{
  if (m_data.p && m_data.n)
  {
    for (bigint_uint_t* ptr = m_data.p, *pstop = m_data.p + m_data.n; ptr < pstop; ++ptr)
    {
      if (*ptr != 0)
        return false;
    }
  }

  return true;
}

//-----------------------------------------------------------------------------
// Returns true if the number is negative
bool Bigint::isNegative() const
{
  return m_data.s < 0;
}

//-----------------------------------------------------------------------------
// Constructs a #Bigint from the binary representation
Bigint Bigint::fromBinary(const char* data, size_t cbData)
{
  if (!data || !cbData)
    return Bigint();

  Bigint rv;
  throw_if_failed(mbedtls::mpi_read_binary(&rv.m_data, reinterpret_cast<const unsigned char*>(data), cbData));
  return std::move(rv);
}

//-----------------------------------------------------------------------------
// Converts a #Bigint to the binary representation
std::string Bigint::toBinary() const
{
  size_t required = mbedtls::mpi_size(&m_data);
  std::string rv(required, '\0');
  if (required)
    throw_if_failed(mbedtls::mpi_write_binary(&m_data, reinterpret_cast<unsigned char*>(&rv.front()), required));

  return std::move(rv);
}

//-----------------------------------------------------------------------------
// Converts a #Bigint to the text representation
std::string Bigint::toString(int radix) const
{
  size_t written = 0;
  size_t available = 0;
  std::string rv;

  int errCode = mbedtls::mpi_write_string<char>(&m_data, radix, nullptr, 0, &written);
  if (errCode == BIGINT_ERR_MPI_BUFFER_TOO_SMALL)
  {
    available = written;
    rv.resize(available);
    errCode = mbedtls::mpi_write_string<char>(&m_data, radix, &rv.front(), available, &written);
  }
  throw_if_failed(errCode);

  if (written < 1)
    throw std::logic_error("Invalid text size");

  rv.resize(written - 1);

  return std::move(rv);
}

//-----------------------------------------------------------------------------
template <typename Tint>
Tint Bigint::mbedtls::extract_int(const Data* x, bool* ok)
{
  if (!x)
  {
    if (ok)
      *ok = false;
    return 0;
  }

  static_assert(sizeof(bigint_sint_t) * 2 >= sizeof(Tint), "The bigint internal data should be at least 32 bits long");

  const size_t bitlen = mpi_bitlen(x);
  const size_t maxBitLen = std::numeric_limits<Tint>::is_signed
      ? sizeof(Tint) * 8 - ((x->s < 0) ? 0 : 1)
      : sizeof(Tint) * 8;

  bool isValid = true;
  if (bitlen > maxBitLen)
  {
    isValid = false;
  }
  else if (bitlen == maxBitLen)
  {
    if (sizeof(bigint_sint_t) >= sizeof(Tint))
    {
      if (std::numeric_limits<Tint>::is_signed)
      {
        if (bigint_uint_t((std::numeric_limits<Tint>::min() + 1) * -1) < x->p[0] - 1)
          isValid = false;
      }
      else if (bigint_uint_t(std::numeric_limits<Tint>::max()) < x->p[0])
      {
        isValid = false;
      }
    }
    else
    {
      Bigint smallest = Bigint::fromInt(std::numeric_limits<Tint>::min());
      if (mpi_cmp_mpi(x, &smallest.m_data) < 0)
      {
        isValid = false;
      }
      else
      {
        Bigint biggest = Bigint::fromInt(std::numeric_limits<Tint>::max());
        if (mpi_cmp_mpi(x, &biggest.m_data) > 0)
          isValid = false;
      }
    }
  }

  if (isValid && !std::numeric_limits<Tint>::is_signed && x->s < 0)
    isValid = false;

  if (!isValid)
  {
    if (ok)
      *ok = false;

    return 0;
  }

  union
  {
    Tint intVal;
    char bytes[sizeof(Tint)];
  } rv;

  memset(rv.bytes, 0, sizeof(Tint));
  memcpy(rv.bytes, x->p, (bitlen + 7) / 8);
  if (std::numeric_limits<Tint>::is_signed
      && x->s < 0
      && std::numeric_limits<Tint>::min() != rv.intVal)
  {
    rv.intVal *= -1;
  }

  if (ok)
    *ok = true;
  return rv.intVal;
}


//-----------------------------------------------------------------------------
template <typename Tint>
Bigint Bigint::mbedtls::from_int(Tint v)
{
  Bigint rv;

  bool oneAdded = false;

  if (std::numeric_limits<Tint>::is_signed)
  {
    if (v == std::numeric_limits<Tint>::min())
    {
      oneAdded = true;
      v += 1;
    }
    if (v < 0)
    {
      v *= -1;
      rv.m_data.s = -1;
    }
  }

  if (sizeof(Tint) <= sizeof(bigint_sint_t))
    throw_if_failed(mpi_grow(&rv.m_data, 1));
  else
    throw_if_failed(mpi_grow(&rv.m_data, 2));

  memcpy(rv.m_data.p, &v, sizeof(v));

  if (oneAdded)
    rv -= 1;

  return std::move(rv);
}

//-----------------------------------------------------------------------------
// Converts to the native (for Bigint) integer representation
bigint_sint_t Bigint::toInt(bool* ok) const
{
  return mbedtls::extract_int<bigint_sint_t>(&m_data, ok);
}

//-----------------------------------------------------------------------------
// Converts to the int16_t representation
int16_t Bigint::toInt16(bool* ok) const
{
  return mbedtls::extract_int<int16_t>(&m_data, ok);
}

//-----------------------------------------------------------------------------
// Creates Bigint from int16_t
Bigint Bigint::fromInt(int16_t v)
{
  return mbedtls::from_int(v);
}

//-----------------------------------------------------------------------------
// Converts to the uint16_t representation
uint16_t Bigint::toUInt16(bool* ok) const
{
  return mbedtls::extract_int<uint16_t>(&m_data, ok);
}

//-----------------------------------------------------------------------------
// Creates Bigint from uint16_t
Bigint Bigint::fromInt(uint16_t v)
{
  return mbedtls::from_int(v);
}

//-----------------------------------------------------------------------------
// Converts to the int32_t representation
int32_t Bigint::toInt32(bool* ok) const
{
  return mbedtls::extract_int<int32_t>(&m_data, ok);
}

//-----------------------------------------------------------------------------
// Creates Bigint from int32_t
Bigint Bigint::fromInt(int32_t v)
{
  return mbedtls::from_int(v);
}

//-----------------------------------------------------------------------------
// Converts to the uint32_t representation
uint32_t Bigint::toUInt32(bool* ok) const
{
  return mbedtls::extract_int<uint32_t>(&m_data, ok);
}

//-----------------------------------------------------------------------------
// Creates Bigint from uint32_t
Bigint Bigint::fromInt(uint32_t v)
{
  return mbedtls::from_int(v);
}

//-----------------------------------------------------------------------------
// Converts to the int64_t representation
int64_t Bigint::toInt64(bool* ok) const
{
  return mbedtls::extract_int<int64_t>(&m_data, ok);
}

//-----------------------------------------------------------------------------
// Creates Bigint from int64_t
Bigint Bigint::fromInt(int64_t v)
{
  return mbedtls::from_int(v);
}

//-----------------------------------------------------------------------------
// Converts to the uint64_t representation
uint64_t Bigint::toUInt64(bool* ok) const
{
  return mbedtls::extract_int<uint64_t>(&m_data, ok);
}

//-----------------------------------------------------------------------------
// Creates Bigint from uint64_t
Bigint Bigint::fromInt(uint64_t v)
{
  return mbedtls::from_int(v);
}

//-----------------------------------------------------------------------------
// Return the number of bits up to and including the most significant '1' bit'
size_t Bigint::bitlen() const
{
  return mbedtls::mpi_bitlen(&m_data);
}

//-----------------------------------------------------------------------------
// Return the number of zero-bits before the least significant '1' bit
size_t Bigint::lsb() const
{
  return mbedtls::mpi_lsb(&m_data);
}

//-----------------------------------------------------------------------------
// Set a bit to a specific value of 0 or 1
void Bigint::setBit(size_t pos, bool val)
{
  throw_if_failed(mbedtls::mpi_set_bit(&m_data, pos, val ? 1 : 0));
}

//-----------------------------------------------------------------------------
// Get a specific bit
bool Bigint::getBit(size_t pos) const
{
  return mbedtls::mpi_get_bit(&m_data, pos) ? 1 : 0;
}

#ifdef QT_CORE_LIB

//-----------------------------------------------------------------------------
// Converts a #Bigint to the binary representation
QByteArray Bigint::toQByteArray() const
{
  size_t required = mbedtls::mpi_size(&m_data);
  QByteArray rv(static_cast<int>(required), '\0');
  if (required)
    throw_if_failed(mbedtls::mpi_write_binary(&m_data, reinterpret_cast<unsigned char*>(rv.data()), required));

  return std::move(rv);
}

//-----------------------------------------------------------------------------
// Converts a #Bigint to the text representation
QString Bigint::toQString(int radix) const
{
  size_t written = 0;
  size_t available = 0;
  QString rv;

  int errCode = mbedtls::mpi_write_string<QChar>(&m_data, radix, nullptr, 0, &written);
  if (errCode == BIGINT_ERR_MPI_BUFFER_TOO_SMALL)
  {
    available = written;
    rv.resize(static_cast<int>(available));
    errCode = mbedtls::mpi_write_string<QChar>(&m_data, radix, rv.data(), available, &written);
  }
  throw_if_failed(errCode);

  if (written < 1)
    throw std::logic_error("Invalid text size");

  rv.resize(static_cast<int>(written - 1));

  return std::move(rv);
}

//-----------------------------------------------------------------------------
// Converts a #Bigint to the QBitArray
QBitArray Bigint::toQBitArray() const
{
  size_t bits = mbedtls::mpi_bitlen(&m_data);

  QBitArray rv(static_cast<int>(bits), false);

  for (size_t n = 0; n < bits; ++n)
  {
    if (mbedtls::mpi_get_bit(&m_data, n))
      rv.setBit(static_cast<int>(n), true);
  }
  return std::move(rv);
}

#endif

//-----------------------------------------------------------------------------
// Compares this number and `other` number
int Bigint::compare(const Bigint& other) const
{
  return mbedtls::mpi_cmp_mpi(&m_data, &other.m_data);
}

//-----------------------------------------------------------------------------
// Compares this number and `other` integer
int Bigint::compare(bigint_sint_t other) const
{
  return mbedtls::mpi_cmp_int(&m_data, other);
}

//-----------------------------------------------------------------------------
// Division: `this = result.first * denominator + result.second`
std::pair<Bigint, Bigint> Bigint::div(const Bigint& denominator) const
{
  std::pair<Bigint, Bigint> rv;
  throw_if_failed(mbedtls::mpi_div_mpi(&rv.first.m_data, &rv.second.m_data, &m_data, &denominator.m_data));

  return std::move(rv);
}

//-----------------------------------------------------------------------------
// Division: `this = result.first * denominator + result.second`
std::pair<Bigint, bigint_sint_t> Bigint::div(bigint_sint_t denominator) const
{
  std::pair<Bigint, bigint_sint_t> rv;
  Bigint rem;
  throw_if_failed(mbedtls::mpi_div_int(&rv.first.m_data, &rem.m_data, &m_data, denominator));
  rv.second = mbedtls::extract_int<bigint_sint_t>(&rem.m_data, nullptr);

  return std::move(rv);
}

//-----------------------------------------------------------------------------
Bigint Bigint::operator /(const Bigint& denominator) const
{
  Bigint rv;
  throw_if_failed(mbedtls::mpi_div_mpi(&rv.m_data, nullptr, &m_data, &denominator.m_data));

  return std::move(rv);
}

//-----------------------------------------------------------------------------
Bigint Bigint::operator /(bigint_sint_t denominator) const
{
  Bigint rv;
  throw_if_failed(mbedtls::mpi_div_int(&rv.m_data, nullptr, &m_data, denominator));

  return std::move(rv);
}

//-----------------------------------------------------------------------------
Bigint Bigint::operator %(const Bigint& denominator) const
{
  Bigint rv;
  throw_if_failed(mbedtls::mpi_mod_mpi(&rv.m_data, &m_data, &denominator.m_data));

  return std::move(rv);
}

//-----------------------------------------------------------------------------
bigint_sint_t Bigint::operator %(bigint_sint_t denominator) const
{
  bigint_uint_t rv;
  throw_if_failed(mbedtls::mpi_mod_int(&rv, &m_data, denominator));

  return static_cast<bigint_sint_t>(rv);
}

//-----------------------------------------------------------------------------
Bigint Bigint::operator *(const Bigint& multiplicator) const
{
  Bigint rv;
  throw_if_failed(mbedtls::mpi_mul_mpi(&rv.m_data, &m_data, &multiplicator.m_data));

  return std::move(rv);
}

//-----------------------------------------------------------------------------
Bigint Bigint::operator *(bigint_sint_t multiplicator) const
{
  Bigint rv;
  throw_if_failed(mbedtls::mpi_mul_int(&rv.m_data, &m_data, multiplicator));

  return std::move(rv);
}

//-----------------------------------------------------------------------------
Bigint Bigint::operator +(const Bigint& other) const
{
  Bigint rv;
  throw_if_failed(mbedtls::mpi_add_mpi(&rv.m_data, &m_data, &other.m_data));

  return std::move(rv);
}

//-----------------------------------------------------------------------------
Bigint Bigint::operator +(bigint_sint_t other) const
{
  Bigint rv;
  throw_if_failed(mbedtls::mpi_add_int(&rv.m_data, &m_data, other));

  return std::move(rv);
}

//-----------------------------------------------------------------------------
Bigint Bigint::operator -(const Bigint& other) const
{
  Bigint rv;
  throw_if_failed(mbedtls::mpi_sub_mpi(&rv.m_data, &m_data, &other.m_data));

  return std::move(rv);
}

//-----------------------------------------------------------------------------
Bigint Bigint::operator -(bigint_sint_t other) const
{
  Bigint rv;
  throw_if_failed(mbedtls::mpi_sub_int(&rv.m_data, &m_data, other));

  return std::move(rv);
}

//-----------------------------------------------------------------------------
Bigint& Bigint::operator <<= (size_t count)
{
  throw_if_failed(mbedtls::mpi_shift_l(&m_data, count));
  return *this;
}

//-----------------------------------------------------------------------------
Bigint& Bigint::operator >>= (size_t count)
{
  throw_if_failed(mbedtls::mpi_shift_r(&m_data, count));
  return *this;
}

//-----------------------------------------------------------------------------
Bigint Bigint::operator -() const
{
  Bigint rv(*this);
  rv.m_data.s *= -1;
  return std::move(rv);
}

//-----------------------------------------------------------------------------
Bigint Bigint::operator ~() const
{
  Bigint rv(*this);

  size_t bitCount = mbedtls::mpi_bitlen(&rv.m_data);
  size_t fullLimbs = (bitCount) / biL;

  // First cycle: process whole limbs
  for (size_t n = 0, cnt = fullLimbs; n < cnt; ++n)
    rv.m_data.p[n] = ~rv.m_data.p[n];

  if (bitCount % biL)
  {
    // Second cycle: process last limb
    bigint_uint_t xorMask = std::numeric_limits<bigint_uint_t>::max()
        >> int(biL - (bitCount - fullLimbs * biL));

    rv.m_data.p[fullLimbs] ^= xorMask;
  }

  return std::move(rv);
}

//-----------------------------------------------------------------------------
Bigint& Bigint::operator ^=(const Bigint& v)
{
  for (size_t n = 0, cnt = std::min(m_data.n, v.m_data.n); n < cnt; ++n)
    m_data.p[n] ^= v.m_data.p[n];
  return *this;
}

//-----------------------------------------------------------------------------
Bigint& Bigint::operator ^=(bigint_uint_t v)
{
  if (m_data.n >= 1)
    *m_data.p ^= v;
  return *this;
}

//-----------------------------------------------------------------------------
Bigint& Bigint::operator |=(const Bigint& v)
{
  for (size_t n = 0, cnt = std::min(m_data.n, v.m_data.n); n < cnt; ++n)
    m_data.p[n] |= v.m_data.p[n];
  return *this;
}

//-----------------------------------------------------------------------------
Bigint& Bigint::operator |=(bigint_uint_t v)
{
  if (m_data.n >= 1)
    *m_data.p |= v;
  return *this;
}

//-----------------------------------------------------------------------------
Bigint& Bigint::operator &=(const Bigint& v)
{
  for (size_t n = 0, cnt = std::min(m_data.n, v.m_data.n); n < cnt; ++n)
    m_data.p[n] &= v.m_data.p[n];
  return *this;
}

//-----------------------------------------------------------------------------
Bigint& Bigint::operator &=(bigint_uint_t v)
{
  if (m_data.n >= 1)
    *m_data.p &= v;
  return *this;
}

#ifdef FULL_BIGINT_LIB

//-----------------------------------------------------------------------------
// Unsigned addition: `result = |this| + |other|`
Bigint Bigint::addAbs(const Bigint& other) const
{
  Bigint rv;
  throw_if_failed(mbedtls::mpi_add_abs(&rv.m_data, &m_data, &other.m_data));

  return std::move(rv);
}

//-----------------------------------------------------------------------------
// Unsigned subtraction: `result = |this| - |other|`
Bigint Bigint::subAbs(const Bigint& other) const
{
  Bigint rv;
  throw_if_failed(mbedtls::mpi_sub_abs(&rv.m_data, &m_data, &other.m_data));

  return std::move(rv);
}

//-----------------------------------------------------------------------------
// Sliding-window exponentiation: result = A^E mod N
Bigint Bigint::expMod(const Bigint& E, const Bigint& N, Bigint* _RR) const
{

  Bigint rv;
  throw_if_failed(mbedtls::mpi_exp_mod(&rv.m_data, &m_data, &E.m_data, &N.m_data, _RR ? &_RR->m_data : nullptr));

  return std::move(rv);
}

//-----------------------------------------------------------------------------
// Makes a Bignumber filled with size bytes of random
Bigint Bigint::fillRandom(size_t size, int (*f_rng)(void*, unsigned char*, size_t), void* p_rng)
{
  Bigint rv;
  throw_if_failed(mbedtls::mpi_fill_random(&rv.m_data, size, f_rng, p_rng));

  return std::move(rv);
}

//-----------------------------------------------------------------------------
// Greatest common divisor `result = gcd(this, other)`
Bigint Bigint::gcd(const Bigint& other) const
{
  Bigint rv;
  throw_if_failed(mbedtls::mpi_gcd(&rv.m_data, &m_data, &other.m_data));

  return std::move(rv);
}

//-----------------------------------------------------------------------------
// Modular inverse: `result = this^-1 mod N`
Bigint Bigint::invMod(const Bigint& N) const
{
  Bigint rv;
  throw_if_failed(mbedtls::mpi_inv_mod(&rv.m_data, &m_data, &N.m_data));

  return std::move(rv);
}

//-----------------------------------------------------------------------------
// Miller-Rabin primality test
bool Bigint::isPrime(int (*f_rng)(void*, unsigned char*, size_t), void* p_rng) const
{
  int rv = mbedtls::mpi_is_prime(&m_data, f_rng, p_rng);
  if (rv == BIGINT_ERR_MPI_NOT_ACCEPTABLE)
    return false;

  throw_if_failed(rv);

  return true;
}


//-----------------------------------------------------------------------------
// Prime number generation
Bigint Bigint::genPrime(size_t nbits, int dh_flag, int (*f_rng)(void*, unsigned char*, size_t), void* p_rng)
{
  Bigint rv;
  throw_if_failed(mbedtls::mpi_gen_prime(&rv.m_data, nbits, dh_flag, f_rng, p_rng));

  return std::move(rv);
}

//-----------------------------------------------------------------------------
// Safe conditional assignement `this = other if cond is true`
void Bigint::condAssign(unsigned char cond, const Bigint& other)
{
  throw_if_failed(mbedtls::mpi_safe_cond_assign(&m_data, &other.m_data, cond));
}

//-----------------------------------------------------------------------------
// Safe conditional `swap X <-> Y if cond is true`
void Bigint::condSwap(unsigned char cond, Bigint& other)
{
  throw_if_failed(mbedtls::mpi_safe_cond_swap(&m_data, &other.m_data, cond));
}

#endif
