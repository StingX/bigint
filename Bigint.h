/** \file Bigint.h
  * \brief Bigint large numbers class definition
  * \author Devyatnikov A.
  * \date 04.06.2016
  * Project: Bigint
  *
  * Licensed under the Apache License, Version 2.0 (the "License"); you may
  *  not use this file except in compliance with the License.
  *  You may obtain a copy of the License at
  *
  *  http://www.apache.org/licenses/LICENSE-2.0
  *
  *  Unless required by applicable law or agreed to in writing, software
  *  distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
  *  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  *  See the License for the specific language governing permissions and
  *  limitations under the License.
  *
  *  This implementation based on https://tls.mbed.org
  */

#ifndef BIGINT_H
#define BIGINT_H

// This Qt stuff works only if the library is compiled with the Qt !!!
#ifdef QT_CORE_LIB

# include <QDebug>
# include <QDataStream>

#endif

#include <string>
#include <cstdint>
#include <memory>

// Define this to declspec(dllexport) or declspec(dllimport) to import/export
// symbols dynamically in windows.
#ifndef BIGINT_EXPORT
# define BININT_EXPORT
#endif

// Define this for header and source file to enable support of cryptographic
// special functions (primes)
#define FULL_BIGINT_LIB

// Undefine this if you wish to disable ASM support in Bigint.cpp
#define BIGINT_HAVE_ASM

/*
 * Define the base integer type, architecture-wise.
 *
 * 32-bit integers can be forced on 64-bit arches (eg. for testing purposes)
 * by defining BIGINT_HAVE_INT32 and undefining BIGINT_HAVE_ASM
 */
#if ( ! defined(BIGINT_HAVE_INT32) && \
        defined(_MSC_VER) && defined(_M_X64) )
  #define BIGINT_HAVE_INT64
  typedef  int64_t bigint_sint_t;
  typedef uint64_t bigint_uint_t;
#else
  #if ( ! defined(BIGINT_HAVE_INT32) &&               \
        defined(__GNUC__) && (                          \
        defined(__amd64__) || defined(__x86_64__)    || \
        defined(__ppc64__) || defined(__powerpc64__) || \
        defined(__ia64__)  || defined(__alpha__)     || \
        (defined(__sparc__) && defined(__arch64__))  || \
        defined(__s390x__) || defined(__mips64) ) )
     #define BIGINT_HAVE_INT64
     typedef  int64_t bigint_sint_t;
     typedef uint64_t bigint_uint_t;
     /* mbedtls_t_udbl defined as 128-bit unsigned int */
     typedef unsigned int bigint_udbl_t __attribute__((mode(TI)));
     #define BIGINT_HAVE_UDBL
  #else
     #define BIGINT_HAVE_INT32
     typedef  int32_t bigint_sint_t;
     typedef uint32_t bigint_uint_t;
     typedef uint64_t bigint_udbl_t;
     #define BIGINT_HAVE_UDBL
  #endif /* !BIGINT_HAVE_INT32 && __GNUC__ && 64-bit platform */
#endif /* !BIGINT_HAVE_INT32 && _MSC_VER && _M_AMD64 */

/*
 * Maximum size MPIs are allowed to grow to in number of limbs.
 */
#define BIGINT_MPI_MAX_LIMBS                             10000

#if !defined(BIGINT_MPI_WINDOW_SIZE)
/*
 * Maximum window size used for modular exponentiation. Default: 6
 * Minimum value: 1. Maximum value: 6.
 *
 * Result is an array of ( 2 << BIGINT_MPI_WINDOW_SIZE ) MPIs used
 * for the sliding window calculation. (So 64 by default)
 *
 * Reduction in size, reduces speed.
 */
#define BIGINT_MPI_WINDOW_SIZE                           6        ///< Maximum windows size used.
#endif /* !BIGINT_MPI_WINDOW_SIZE */

#if !defined(BIGINT_MPI_MAX_SIZE)
/*
 * Maximum size of MPIs allowed in bits and bytes for user-MPIs.
 * ( Default: 512 bytes => 4096 bits, Maximum tested: 2048 bytes => 16384 bits )
 *
 * Note: Calculations can results temporarily in larger MPIs. So the number
 * of limbs required (BIGINT_MPI_MAX_LIMBS) is higher.
 */
#define BIGINT_MPI_MAX_SIZE                              1024     ///< Maximum number of bytes for usable MPIs.
#endif /* !BIGINT_MPI_MAX_SIZE */

#define BIGINT_MPI_MAX_BITS                              ( 8 * BIGINT_MPI_MAX_SIZE )    ///< Maximum number of bits for usable MPIs.


class BININT_EXPORT Bigint
{
public:
  /** Constructs an #isNull object */
  Bigint();
  /** Constructs a #Bigint from an interger */
  explicit Bigint(bigint_sint_t i);
  /** Constructs a #Bigint from the string representation */
  Bigint(const char* pText, int radix = 10);
  /** Constructs a #Bigint from the string representation */
  Bigint(const char* pText, size_t cbText, int radix);
  /** Constructs a #Bigint from the string representation */
  Bigint(const std::string&, int radix = 10);
#ifdef QT_CORE_LIB
  /** Constructs a #Bigint from the string representation */
  Bigint(const QString&, int radix = 10);
  /** Constructs a $Bigint from the bit representation */
  Bigint(const QBitArray&);
#endif

  // Object copy and move
  Bigint(const Bigint&);
  Bigint(Bigint&&);
  Bigint& operator = (const Bigint&);
  Bigint& operator = (Bigint&&);

  /** Swaps content of the two classes */
  void swap(Bigint&);

  /** Swaps content of the two classes */
  inline void swap(Bigint&& v) { swap(v); }

  /** Provides hashing for std::unordered_map and std::unordered_set */
  size_t hash() const;

  /** Dtor */
  ~Bigint();

public: // Most generic information functions

  /** Returns true if this class #isNull or contains a value of zero */
  bool isEmpty() const;

  /** Returns true if the number is negative */
  bool isNegative() const;

public: // Data input/output functions
  /** Constructs a #Bigint from the binary representation
   * \note This function DOES NOT RESTORE THE SIGN
   */
  static Bigint fromBinary(const char* data, size_t cbData);

  /** Constructs a #Bigint from the binary representation
   * \note This function DOES NOT RESTORE THE SIGN
   */
  static inline Bigint fromBinary(const std::string& v) { return fromBinary(v.data(), v.size()); }

  /** Converts a #Bigint to the binary representation
   * \note This function DOES NOT STORE THE SIGN
   */
  std::string toBinary() const;

  /** Converts a #Bigint to the text representation */
  std::string toString(int radix = 10) const;

  /** Converts to the native (for Bigint) integer representation
   * \param ok       Returned value with a status of operation: true - the Bigint
   *                 successfully converted, false - Bigint could not fit to the
   *                 integer.
   */
  bigint_sint_t toInt(bool* ok = nullptr) const;

  /** Converts to the int16_t representation
   * \param ok       Returned value with a status of operation: true - the Bigint
   *                 successfully converted, false - Bigint could not fit to the
   *                 integer.
   */
  int16_t toInt16(bool* ok = nullptr) const;

  /** Creates Bigint from int16_t */
  static Bigint fromInt(int16_t);

  /** Converts to the uint16_t representation
   * \param ok       Returned value with a status of operation: true - the Bigint
   *                 successfully converted, false - Bigint could not fit to the
   *                 integer.
   */
  uint16_t toUInt16(bool* ok = nullptr) const;

  /** Creates Bigint from uint16_t */
  static Bigint fromInt(uint16_t);

  /** Converts to the int32_t representation
   * \param ok       Returned value with a status of operation: true - the Bigint
   *                 successfully converted, false - Bigint could not fit to the
   *                 integer.
   */
  int32_t toInt32(bool* ok = nullptr) const;

  /** Creates Bigint from int32_t */
  static Bigint fromInt(int32_t);

  /** Converts to the uint32_t representation
   * \param ok       Returned value with a status of operation: true - the Bigint
   *                 successfully converted, false - Bigint could not fit to the
   *                 integer.
   */
  uint32_t toUInt32(bool* ok = nullptr) const;

  /** Creates Bigint from uint32_t */
  static Bigint fromInt(uint32_t);

  /** Converts to the int64_t representation
   * \param ok       Returned value with a status of operation: true - the Bigint
   *                 successfully converted, false - Bigint could not fit to the
   *                 integer.
   */
  int64_t toInt64(bool* ok = nullptr) const;

  /** Creates Bigint from int64_t */
  static Bigint fromInt(int64_t);

  /** Converts to the uint64_t representation
   * \param ok       Returned value with a status of operation: true - the Bigint
   *                 successfully converted, false - Bigint could not fit to the
   *                 integer.
   */
  uint64_t toUInt64(bool* ok = nullptr) const;

  /** Creates Bigint from uint64_t */
  static Bigint fromInt(uint64_t);

  /** Return the number of bits up to and including the most significant '1' bit'
   *
   * Note: Thus also the one-based index of the most significant '1' bit
   */
  size_t bitlen() const;

  /** Return the number of zero-bits before the least significant '1' bit
   *
   * Note: Thus also the zero-based index of the least significant '1' bit
   */
  size_t lsb() const;

  /** Set a bit to a specific value of 0 or 1
   *
   * \note           Will grow X if necessary to set a bit to 1 in a not yet
   *                 existing limb. Will not grow if bit should be set to 0
   *
   * \param pos      Zero-based index of the bit
   * \param val      The value to set the bit to (0 or 1)
   */
  void setBit(size_t pos, bool val);

  /** Get a specific bit */
  bool getBit(size_t pos) const;

#ifdef QT_CORE_LIB
  /** Constructs a #Bigint from the binary representation
   *
   * \note This function DOES NOT RESTORE THE SIGN
   */
  static inline Bigint fromQByteArray(const QByteArray& v) { return fromBinary(v.data(), static_cast<size_t>(v.size())); }

  /** Converts a #Bigint to the binary representation
   *
   * \note This function DOES NOT STORE THE SIGN
   */
  QByteArray toQByteArray() const;

  /** Converts a #Bigint to the text representation */
  QString toQString(int radix = 10) const;

  /** Converts a #Bigint to the QBitArray */
  QBitArray toQBitArray() const;
#endif

public: // Comparision
  /** Compares this number and `other` number
   * \return
   *      - -1 - If this number is less than `other`
   *      - 0 - If both numbers are equal
   *      - 1 - if this number is greater than `other`
   */
  int compare(const Bigint& other) const;

  /** Compares this number and `other` integer
   * \return
   *      - -1 - If this number is less than `other`
   *      - 0 - If both numbers are equal
   *      - 1 - if this number is greater than `other`
   */
  int compare(bigint_sint_t other) const;

  inline bool operator == (const Bigint& v) const { return compare(v) == 0; }
  inline bool operator != (const Bigint& v) const { return compare(v) != 0; }
  inline bool operator < (const Bigint& v) const { return compare(v) < 0; }
  inline bool operator <= (const Bigint& v) const { return compare(v) <= 0; }
  inline bool operator > (const Bigint& v) const { return compare(v) > 0; }
  inline bool operator >= (const Bigint& v) const { return compare(v) >= 0; }
  inline bool operator !() const { return isEmpty(); }

public: // Mathematics
  /** Division: `this = result.first * denominator + result.second`
   * \returns
   * - first - quotient of the integral division operation
   * - second - remainder of the integral division operation
   */
  std::pair<Bigint, Bigint> div(const Bigint& denominator) const;

  /** Division: `this = result.first * denominator + result.second`
   * \returns
   * - first - quotient of the integral division operation
   * - second - remainder of the integral division operation
   */
  std::pair<Bigint, bigint_sint_t> div(bigint_sint_t denominator) const;

  // Division operators
  Bigint operator /(const Bigint&) const;
  Bigint operator /(bigint_sint_t) const;
  inline Bigint& operator /=(const Bigint& v) { swap(operator/(v)); return *this; }
  inline Bigint& operator /=(bigint_sint_t v) { swap(operator/(v)); return *this; }

  // Modulo operators
  Bigint operator %(const Bigint&denominator) const;
  bigint_sint_t operator %(bigint_sint_t) const;
  inline Bigint& operator %=(const Bigint& v) { swap(operator%(v)); return *this; }
  inline Bigint& operator %=(bigint_sint_t v) { swap(operator%(Bigint(v))); return *this; }

  // Multiplication operators
  Bigint operator *(const Bigint&) const;
  Bigint operator *(bigint_sint_t) const;
  inline Bigint& operator *=(const Bigint& v) { swap(operator*(v)); return *this; }
  inline Bigint& operator *=(bigint_sint_t v) { swap(operator*(v)); return *this; }

  // Addition operators
  Bigint operator +(const Bigint&) const;
  Bigint operator +(bigint_sint_t) const;
  Bigint& operator +=(const Bigint& v) { swap(operator+(v)); return *this; }
  Bigint& operator +=(bigint_sint_t v) { swap(operator+(v)); return *this; }
  inline Bigint& operator ++() { swap(operator+(bigint_sint_t(1))); return *this; }
  inline Bigint operator ++(int) { Bigint rv = *this; swap(operator+(bigint_sint_t(1))); return std::move(rv); }

  // Subtraction operators
  Bigint operator -(const Bigint&) const;
  Bigint operator -(bigint_sint_t) const;
  inline Bigint& operator -=(const Bigint& v) { swap(operator-(v)); return *this; }
  inline Bigint& operator -=(bigint_sint_t v) { swap(operator-(v)); return *this; }
  inline Bigint& operator --() { swap(operator-(bigint_sint_t(1))); return *this; }
  inline Bigint operator --(int) { Bigint rv = *this; swap(operator-(bigint_sint_t(1))); return std::move(rv); }

  // Bit shift (does not change a sign)
  inline Bigint operator <<(size_t v) const { return std::move(Bigint(*this) <<= v); }
  inline Bigint operator <<(int v) const { return std::move(Bigint(*this) <<= size_t(v)); }
  Bigint& operator <<=(size_t);
  inline Bigint& operator <<=(int v) { return operator <<=(size_t(v)); }
  inline Bigint operator >>(size_t v) const { return std::move(Bigint(*this) >>= v); }
  inline Bigint operator >>(int v) const { return std::move(Bigint(*this) >>= size_t(v)); }
  Bigint& operator >>=(size_t);
  inline Bigint& operator >>=(int v) { return operator >>=(size_t(v)); }

  // Unary operators
  /** Negate this number */
  Bigint operator -() const;
  /** Noop */
  Bigint operator +() const { return Bigint(*this); }
  /** Inverts all the bits in this number. Does not affect sign of the number */
  Bigint operator ~() const;

  // Bitwise operators (DOES NOT AFFECT SIGN)
  inline Bigint operator ^(const Bigint& v) const { return std::move(Bigint(*this) ^= v); }
  Bigint& operator ^=(const Bigint& v);
  inline Bigint operator ^(bigint_uint_t v) const { return std::move(Bigint(*this) ^= v); }
  Bigint& operator ^=(bigint_uint_t v);

  inline Bigint operator &(const Bigint& v) const { return std::move(Bigint(*this) &= v); }
  Bigint& operator &=(const Bigint& v);
  inline Bigint operator &(bigint_uint_t v) const { return std::move(Bigint(*this) &= v); }
  Bigint& operator &=(bigint_uint_t v);

  inline Bigint operator |(const Bigint& v) const { return std::move(Bigint(*this) |= v); }
  Bigint& operator |=(const Bigint& v);
  inline Bigint operator |(bigint_uint_t v) const { return std::move(Bigint(*this) |= v); }
  Bigint& operator |=(bigint_uint_t v);

// Additional "int" operator overloads for 64 bit
#ifndef BIGINT_HAVE_INT32
  inline Bigint operator /(int v) const { return operator / (bigint_sint_t(v)); }
  inline Bigint& operator /=(int v) { return operator /= (bigint_sint_t(v)); }
  inline bigint_sint_t operator %(int v) const { return operator % (bigint_sint_t(v)); }
  inline Bigint& operator %=(int v) { return operator %= (bigint_sint_t(v)); }
  inline Bigint operator *(int v) const { return operator * (bigint_sint_t(v)); }
  inline Bigint& operator *=(int v) { return operator *= (bigint_sint_t(v)); }
  inline Bigint operator +(int v) const { return operator + (bigint_sint_t(v)); }
  inline Bigint& operator +=(int v) { return operator += (bigint_sint_t(v)); }
  inline Bigint operator -(int v) const { return operator - (bigint_sint_t(v)); }
  inline Bigint& operator -=(int v) { return operator -= (bigint_sint_t(v)); }
  inline Bigint operator ^(int v) const { return std::move(Bigint(*this) ^= bigint_uint_t(v)); }
  inline Bigint& operator ^=(int v) { return operator ^= (bigint_uint_t(v)); }
  inline Bigint operator &(int v) const { return std::move(Bigint(*this) &= bigint_uint_t(v)); }
  inline Bigint& operator &=(int v) { return operator &= (bigint_uint_t(v)); }
  inline Bigint operator |(int v) const { return std::move(Bigint(*this) |= bigint_uint_t(v)); }
  inline Bigint& operator |=(int v) { return operator |= (bigint_uint_t(v)); }
#endif

#ifdef FULL_BIGINT_LIB
public: // Advanced features not required for regular number operations
  /** Unsigned addition: `result = |this| + |other|` */
  Bigint addAbs(const Bigint& other) const;

  /** Unsigned subtraction: `result = |this| - |other|` */
  Bigint subAbs(const Bigint& other) const;

  /** Sliding-window exponentiation: result = A^E mod N
   *
   * \param E        Exponent MPI
   * \param N        Modular MPI
   * \param _RR      Speed-up MPI used for recalculations
   *
   * \note           _RR is used to avoid re-computing R*R mod N across
   *                 multiple calls, which speeds up things a bit. It can
   *                 be set to nullptr if the extra performance is unneeded.
   */
  Bigint expMod(const Bigint& E, const Bigint& N, Bigint* _RR = nullptr) const;

  /** Makes a Bignumber filled with size bytes of random
   *
   * \param size     Size in bytes
   * \param f_rng    RNG function
   * \param p_rng    RNG parameter
   */
  static Bigint fillRandom(size_t size,
                           int (*f_rng)(void *, unsigned char *, size_t),
                           void *p_rng );

  /** Greatest common divisor `result = gcd(this, other)` */
  Bigint gcd(const Bigint&) const;

  /** Modular inverse: `result = this^-1 mod N` */
  Bigint invMod(const Bigint& N) const;

  /** Miller-Rabin primality test
   *
   * \param f_rng    RNG function
   * \param p_rng    RNG parameter
   *
   * \return
   * - true - probably prime
   * - false - not a prime
   */
  bool isPrime(int (*f_rng)(void *, unsigned char *, size_t),
               void *p_rng) const;

  /** Prime number generation
   *
   * \param nbits    Required size of X in bits
   *                 ( 3 <= nbits <= BIGINT_MPI_MAX_BITS )
   * \param dh_flag  If 1, then (X-1)/2 will be prime too
   * \param f_rng    RNG function
   * \param p_rng    RNG parameter
   */
  static Bigint genPrime(size_t nbits, int dh_flag,
                         int (*f_rng)(void *, unsigned char *, size_t),
                         void *p_rng);

  /** Safe conditional assignement `this = other if cond is true`
   *
   * \param cond     1: perform the assignment, 0: keep original value
   * \param other    Value to be assigned
   *
   * \note           This function is equivalent to
   *                      if (cond) X = Y;
   *                 except that it avoids leaking any information about whether
   *                 the assignment was done or not (the above code may leak
   *                 information through branch prediction and/or memory access
   *                 patterns analysis).
   */
  void condAssign(unsigned char cond, const Bigint& other);

  /** Safe conditional `swap X <-> Y if cond is true`
   *
   * \param cond     1: perform the swap, 0: keep this and other's
   *                 original values
   * \param other    Value to swap with
   *
   * \note           This function is equivalent to
   *                      if (cond) std::swap(X, Y);
   *                 except that it avoids leaking any information about whether
   *                 the assignment was done or not (the above code may leak
   *                 information through branch prediction and/or memory access
   *                 patterns analysis).
   */
  void condSwap(unsigned char cond, Bigint& other);
#endif

private:
  struct Data
  {
    int s;              /*!<  integer sign      */
    size_t n;           /*!<  total # of limbs  */
    bigint_uint_t *p;   /*!<  pointer to limbs  */
  };
  struct mbedtls;
  friend struct mbedtls;
  Data m_data;
};


/** \relates Bigint */
inline bool operator == (const Bigint& left, bigint_sint_t right) { return left.compare(right) == 0; }
/** \relates Bigint */
inline bool operator != (const Bigint& left, bigint_sint_t right) { return left.compare(right) != 0; }
/** \relates Bigint */
inline bool operator < (const Bigint& left, bigint_sint_t right) { return left.compare(right) < 0; }
/** \relates Bigint */
inline bool operator <= (const Bigint& left, bigint_sint_t right) { return left.compare(right) <= 0; }
/** \relates Bigint */
inline bool operator > (const Bigint& left, bigint_sint_t right) { return left.compare(right) > 0; }
/** \relates Bigint */
inline bool operator >= (const Bigint& left, bigint_sint_t right) { return left.compare(right) >= 0; }

/** \relates Bigint */
inline bool operator == (bigint_sint_t left, const Bigint& right) { return right.compare(left) == 0; }
/** \relates Bigint */
inline bool operator != (bigint_sint_t left, const Bigint& right) { return right.compare(left) != 0; }
/** \relates Bigint */
inline bool operator < (bigint_sint_t left, const Bigint& right) { return right.compare(left) >= 0; }
/** \relates Bigint */
inline bool operator <= (bigint_sint_t left, const Bigint& right) { return right.compare(left) > 0; }
/** \relates Bigint */
inline bool operator > (bigint_sint_t left, const Bigint& right) { return right.compare(left) <= 0; }
/** \relates Bigint */
inline bool operator >= (bigint_sint_t left, const Bigint& right) { return right.compare(left) < 0; }

/** \relates Bigint */
inline Bigint operator /(int left, const Bigint& right) { return Bigint(left) / right; }
/** \relates Bigint */
inline Bigint operator %(int left, const Bigint& right) { return Bigint(left) % right; }
/** \relates Bigint */
inline Bigint operator *(int left, const Bigint& right) { return Bigint(left) * right; }
/** \relates Bigint */
inline Bigint operator +(int left, const Bigint& right) { return Bigint(left) + right; }
/** \relates Bigint */
inline Bigint operator -(int left, const Bigint& right) { return Bigint(left) - right; }

#ifndef BIGINT_HAVE_INT32

///** \relates Bigint */
//inline Bigint operator /(const Bigint& left, int right) { return left.operator / (bigint_sint_t(right)); }
///** \relates Bigint */
//inline int operator %(const Bigint& left, int right) { return int(left.operator % (bigint_sint_t(right))); }
///** \relates Bigint */
//inline Bigint operator *(const Bigint& left, int right) { return left.operator *(bigint_sint_t(right)); }
///** \relates Bigint */
//inline Bigint operator +(const Bigint& left, int right) { return left.operator + (bigint_sint_t(right)); }
///** \relates Bigint */
//inline Bigint operator -(const Bigint& left, int right) { return left.operator - (bigint_sint_t(right)); }

#endif

inline void swap(Bigint& a, Bigint& b)
{
  a.swap(b);
}

namespace std
{
   template<>
   struct hash<Bigint>
   {
      size_t operator()(const Bigint& p) const
      {
         return p.hash();
      }
   };
}

#ifdef QT_CORE_LIB

inline QDebug operator <<(QDebug dbg, const Bigint& v)
{
  dbg.nospace() << "Bigint(" << v.toQString(10) << ")";
  return dbg.maybeSpace();
}

inline uint qHash(const Bigint& t, uint seed)
{
  return static_cast<uint>(t.hash()) ^ seed;
}

inline QDataStream& operator << (QDataStream& stream, const Bigint& v)
{
  stream
      << (uint8_t)(v.isNegative() ? 0xFF : 0x00)
      << v.toQByteArray();

  return stream;
}

inline QDataStream& operator >> (QDataStream& stream, Bigint& v)
{
  uint8_t isNegative;
  QByteArray data;

  stream
      >> isNegative
      >> data;

  v.swap(Bigint::fromQByteArray(data));
  if (isNegative != 0x00)
    v *= -1;

  return stream;
}

Q_DECLARE_METATYPE(Bigint)

#endif

#endif//BIGINT_H
